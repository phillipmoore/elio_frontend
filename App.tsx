import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider } from 'react-redux';
import { ToastProvider } from 'react-native-toast-notifications'

import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';
import TaskComponent from './components/TaskComponent';
import store from './store';

export default function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <Provider store={store} >
        <ToastProvider>
          <Navigation colorScheme={colorScheme} />
          <StatusBar />
          <TaskComponent />
        </ToastProvider>
      </Provider>
    );
  }
}
