import { 
  SET_HEADER_HEIGHT,
  SET_PROFILE_CONTENT_HEIGHT,
  SET_BOTTOM_SCREEN_COORDS,
  SET_TOP_SCREEN_COORDS,
  SET_BOTTOM_CONTAINER_OPEN,
  SET_TOP_CONTAINER_OPEN,
  SET_TOP_DATA,
  SET_SCALE_TOP,
  SET_SCALE_BOTTOM,
  SET_PAN_RESPONDER_PARAMS
} from './types'

export const setHeaderHeight = (height: number) => (
  /* header height is the sum of the nav header and profile header heights.
    seperate into two different values if we need later.
    see /components/profileDetail.tsx
  */
  {
    type: SET_HEADER_HEIGHT,
    data: height
  }
)

export const setProfileContentHeight = (height: number) => (
  {
    type: SET_PROFILE_CONTENT_HEIGHT,
    data: height
  }
)

export const setBottomScreenCoords = (object: any) => (
  {
    type: SET_BOTTOM_SCREEN_COORDS,
    data: object
  }
)

export const setTopScreenCoords = (object: any) => (
  {
    type: SET_TOP_SCREEN_COORDS,
    data: object
  }
)

export const setBottomContainerOpen = (bool: boolean) => (
  {
    type: SET_BOTTOM_CONTAINER_OPEN,
    data: bool
  }
)

export const setTopContainerOpen = (bool: boolean) => (
  {
    type: SET_TOP_CONTAINER_OPEN,
    data: bool
  }
)

export const setScaleTop = (value: any) => (
  {
    type: SET_SCALE_TOP,
    data: value
  }
)

export const setScaleBottom = (value: any) => (
  {
    type: SET_SCALE_BOTTOM,
    data: value
  }
)

export const setPanResponderParams = (obj: any) => (
  {
    type: SET_PAN_RESPONDER_PARAMS,
    data: obj
  }
)

