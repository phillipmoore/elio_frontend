import {SET_IG_RESOURCE} from './types';

import {storeData, getData} from '../utils'

export const setIgResource = (resource: object) => {
	storeData('@igResource', resource)
  return (
    {
      type: SET_IG_RESOURCE,
      data: resource
    }
  )
}
