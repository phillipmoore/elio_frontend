import { 
  SET_INITIAL_TOKENS, 
  REMOVE_INITIAL_TOKENS,
  SET_USER,
  REMOVE_USER,
  SET_PROFILE,
  REMOVE_PROFILE,
  SET_UPDATE,
  SET_NOTIFICATIONS, 
  SET_USER_LOCATION,
  SET_INSTAGRAM_OBJECT,
  SET_TOP_DATA 
} from './types'

import {storeData, getData} from '../utils'

export const setInitialTokens = (tokenObject: object) => {
  storeData('@initialTokens', tokenObject)
  return (
    {
      type: SET_INITIAL_TOKENS,
      data: tokenObject
    }
  )
};

export const removeInitialTokens = () => {
  storeData('@initialTokens', {})
  return (
    {
      type: REMOVE_INITIAL_TOKENS
    }
  )
};

export const setUser = (user: object) => {
  storeData('@user', user)
  return (
    {
      type: SET_USER,
      data: user
    }
  )
}

export const removeUser = () => {
  storeData('@user', {})
  return (
    {
      type: REMOVE_USER
    }
  )
};

export const setProfile = (profile: object) => {
  storeData('@profile', profile)
  return (
    {
      type: SET_PROFILE,
      data: profile
    }
  )
}

export const removeProfile = () => {
  storeData('@profile', {})
  return (
    {
      type: REMOVE_PROFILE
    }
  )
};

export const setUpdate = (date: Date) => {
  return (
    {
      type: SET_UPDATE,
      data: date
    }
  )
}

export const setNotifications = (notifications: object) => {
  return (
    {
      type: SET_NOTIFICATIONS,
      data: notifications
    }
  )
}

export const setUserLocation = (location: object) => {
  return (
    {
      type: SET_USER_LOCATION,
      data: location
    }
  )
}

export const setInstagramObject = (obj: object) => {
  return (
    {
      type: SET_INSTAGRAM_OBJECT,
      data: obj
    }
  )
}

export const setTopData = (data: any) => {
  return (
    {
      type: SET_TOP_DATA,
      data: data
    }
  )
}