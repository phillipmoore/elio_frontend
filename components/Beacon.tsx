import React, { useEffect, useRef } from 'react';
import { Animated, StyleSheet } from 'react-native';
import { View } from '../components/Themed';
import Dot from '../components/Dot';


const Beacon = () => {
  const pulse = useRef(new Animated.Value(0)).current

  useEffect(() => {
    startPulse()
  }, [])

  const startPulse = () => {
    Animated.sequence([
      Animated.timing(pulse, {
        toValue: 100,
        duration: 2000,
        useNativeDriver: true
      }),
      Animated.timing(pulse, {
        toValue: 0,
        duration: 0,
        useNativeDriver: true
      }),
      Animated.delay(6000),
    ]).start(({finished}) => {
      if (finished) {
        startPulse()
      }
    })
  }

  const getPulseStyle = () => {
    const opacity = pulse.interpolate({
      inputRange: [0, 100],
      outputRange: [1, 0]
    })
    const dim = pulse.interpolate({
      inputRange: [0, 100],
      outputRange: [1, 6]
    })
    
    return { transform: [{ scaleX: dim }, { scaleY: dim }], opacity }
  }

  return <View style={styles.container}>
    <Dot position={styles.dot} />
    <Animated.View pointerEvents="none" style={[styles.main, getPulseStyle()]} />
  </View>
}

export default Beacon

const styles = StyleSheet.create({
  container: {
    height: 6, 
    width: 6,
  },
  dot: {
    position: 'absolute'
  },
  main: {
    height: 6, 
    width: 6, 
    backgroundColor: 'crimson', 
    borderRadius: 3,
    position: 'absolute',
    opacity: 1
  }
})