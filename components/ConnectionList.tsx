import React, { useState } from 'react'
import { Ionicons } from '@expo/vector-icons'
import { FlatList, TouchableOpacity, StyleSheet } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { ListItem, Avatar, Tab, TabView, Badge, Icon } from 'react-native-elements'
import { Text, View } from '../components/Themed'
import SwipeableListItem from '../components/SwipeableListItem'
import Dot from '../components/Dot'
import {
  COMPLETE_CONNECTION,
  SHOW_HIDE_CONNECTION,
  ConnectionInterface
} from '../types/modelTypes'

import { ComponentInterface } from '../types/componentTypes'


const LeftButton = ({ isLoved }: { isLoved: boolean }) => {
  return (isLoved ?
    <Icon
      type="ionicon"
      name="heart-dislike-outline"
      size={24}
    /> :
    <Icon
      type="ionicon"
      name="heart"
      color="crimson"
      size={24}
    />
  )
}

const RightButton = ({ isHidden }: { isHidden: boolean }) => {
  return (!isHidden ?
    <Icon
      type="ionicon"
      name="ios-eye-off-outline"
      size={24}
    /> :
    <Icon
      type="ionicon"
      name="ios-eye-outline"
      size={24}
    />
  )
}

const ConnectionListItem = ({ profile, connection, nestedAction, swipingCheck, fromTabView }: ComponentInterface['ConnectionListItem']) => {
  const [swiping, setSwiping] = useState(false)
  const navigation = useNavigation()
  const { initiator, responder } = connection.item
  const isInitiator =  profile.id === initiator.id
  const otherProfile = !isInitiator ? initiator : responder
  const isLoved = (connection.item.accepted && !isInitiator) || isInitiator
  const connectedKey = (!isLoved && fromTabView) ? 'Unconnected' : ''
  const isHidden = isInitiator ? connection.item.initiator_hidden : connection.item.responder_hidden
  
  const componentType = fromTabView ? SwipeableListItem : View
  
  return (
    <ListItem
      Component={componentType}
      key={otherProfile.id}
      swipingCheck={(swiping: boolean) => swipingCheck(swiping)}
      id={otherProfile.id}
      leftButtonPressed={() => nestedAction({type: COMPLETE_CONNECTION, connection: connection.item })}
      rightButtonPressed={() => nestedAction({type: SHOW_HIDE_CONNECTION, connection: connection.item })}
      leftButton={() => <LeftButton isLoved={isLoved} />}
      rightButton={() => <RightButton isHidden={isHidden} />}
    >
      <TouchableOpacity 
        onPress={() => navigation.navigate('ProfileDetail', { profile: otherProfile })}
        style={[styles.opacityContainer, (isHidden && styles.disabledListItem)]}
        disabled={isHidden}
      >
        <ListItem.Content style={styles.avatarSection}>
          <Avatar 
            source={{uri: otherProfile.avatar}} 
            rounded={true}
            size={'large'}
          />
          {(isLoved && fromTabView) &&
            <Badge
              containerStyle={{ position: 'absolute', bottom: 2, right: 2 }}
              Component={() => <Ionicons name="ios-heart" size={16} color={'crimson'} />}
            /> 
          }
        </ListItem.Content>
        <ListItem.Content style={styles.textSection}>
          <ListItem.Title>
            <Text style={styles[`titleText${connectedKey}`]}>{otherProfile.username}</Text>          
          </ListItem.Title>
          <ListItem.Subtitle>
            <Text style={styles[`subtitleText${connectedKey}`]}>{otherProfile.user.name}</Text>
          </ListItem.Subtitle>
        </ListItem.Content>
          <ListItem.Content>
          {(!(isLoved || isHidden) && fromTabView) &&
            <Dot />
          }
        </ListItem.Content>
        <ListItem.Content  style={styles.iconSection}>
          <TouchableOpacity 
            onPress={() => navigation.navigate('ContextModal', { profile: otherProfile })}
          >
            <Ionicons
              name="ellipsis-horizontal"
              size={25}
              color={'#000'}
            />
          </TouchableOpacity>
        </ListItem.Content>
      </TouchableOpacity>
    </ListItem>
  )
}

const ConnectionList = (props: ComponentInterface['ConnectionList']) => {
  const [index, setIndex] = useState(props.tabIndex)
  const [swiping, setSwiping] = useState(false)
  const { 
    connections, 
    mutualConnections, 
    profile, 
    nestedAction,
    fromTabView } = props

  return ( mutualConnections.length > 0 ?
    <View style={{ height: '100%', overflow: 'hidden' }}>
      <Tab value={index} onChange={setIndex}>
        <Tab.Item title="connections"/>
        <Tab.Item title="mutual" />
      </Tab>

      <TabView value={index} onChange={setIndex}> 
        <TabView.Item style={{ width: '100%'}}>
          <FlatList 
            data={connections}
            renderItem={(connection: ConnectionInterface) => (
              <ConnectionListItem 
                connection={connection} 
                profile={profile} 
                nestedAction={nestedAction} 
                swipingCheck={(swiping: boolean) => setSwiping(swiping)}
                fromTabView={fromTabView}
              />
            )}
          />
        </TabView.Item>
        <TabView.Item style={{ width: '100%' }}>
          <FlatList 
            data={mutualConnections}
            renderItem={(connection: ConnectionInterface) => (
              <ConnectionListItem 
                connection={connection} 
                profile={profile} 
                nestedAction={nestedAction} 
                swipingCheck={(swiping: boolean) => setSwiping(swiping)}
                fromTabView={fromTabView}
              />
            )}
          />
        </TabView.Item>
      </TabView>      
    </View> :
    <FlatList 
      data={connections}
      renderItem={(connection: ConnectionInterface) => ( 
        <ConnectionListItem 
          connection={connection} 
          profile={profile} 
          nestedAction={nestedAction} 
          swipingCheck={(swiping: boolean) => setSwiping(swiping)}
          fromTabView={fromTabView}
        />
      )}
      scrollEnabled={!swiping}
    />
  )
}

export default ConnectionList

const styles = StyleSheet.create({
  opacityContainer: {
    flexDirection: 'row'
  },
  disabledListItem: {
    opacity: 0.5,
  },
  avatarSection: {
    flex: 0,
  },
  textSection: {
    flex: 0,
    paddingHorizontal: 16
  },
  titleText: {
    fontSize: 14,
  },
  subtitleText: {
    color: '#555',
    fontSize: 14,
  },
  titleTextUnconnected: {
    fontSize: 14,
    fontWeight: 'bold'
  },
  subtitleTextUnconnected: {
    color: '#000',
    fontSize: 14,
  },
  alignRight: {
    alignItems: 'flex-end'
  },
  iconSection: {
    flex: 1,
    alignItems: 'flex-end'
  }
})