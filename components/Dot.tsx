import * as React from 'react'
import { StyleSheet } from 'react-native'
import { View } from '../components/Themed'

import { ComponentInterface } from '../types/componentTypes'

const Dot = (props: ComponentInterface['Dot']) =>{
  return <View style={[styles.main, props.position]}></View>
}

export default Dot

const styles = StyleSheet.create({
  main: {
    height: 6, 
    width: 6, 
    backgroundColor: 'crimson', 
    borderRadius: 3,
  }
})