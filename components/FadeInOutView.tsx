import React, { useRef, useEffect } from 'react';
import { Animated } from 'react-native';
import { ComponentInterface } from '../types/componentTypes';

const FadeInOutView = (props: ComponentInterface['FadeInOutView']) => {
  const fadeAnim = useRef(new Animated.Value(0)).current  // Initial value for opacity: 0
  const value = props.isVisible ? 0 : 1

  useEffect(() => {
    Animated.timing(
      fadeAnim,
      {
        toValue: value,
        duration: 500,
        useNativeDriver: true
      },
    ).start();
  }, [fadeAnim, props.isVisible])

  return (
    <Animated.View              
      style={{
        ...props.style,
        opacity: fadeAnim, // Bind opacity to animated value
      }}

    >
      {props.children}
    </Animated.View>
  );
}

export default FadeInOutView