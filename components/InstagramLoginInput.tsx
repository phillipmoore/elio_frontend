import React from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { Text, View } from '../components/Themed'

export default function InstagramLoginInput() {
  const navigation = useNavigation()
  const addInstagram = () => {
    navigation.navigate('WebView')
  }

  return (
    <View style={styles.container}>
      <Text style={styles.label}>Instagram</Text>
      <TouchableOpacity onPress={addInstagram}>
        <Text>Add Instagram to your profile</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    padding: 32
  },
  label: {
    color: 'rgba(0,0,0,0.5)',
    paddingHorizontal: 16,
  }
});