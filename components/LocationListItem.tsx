import axios from 'axios'
import _ from 'lodash'

import React, { useEffect, useState } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { Text, View, DoublePressOpacity } from './Themed'
import { 
  StyleSheet, 
  Dimensions, 
  TouchableOpacity,
  Image
} from "react-native"
import { Ionicons } from '@expo/vector-icons'
import Colors from '../constants/Colors'
import useColorScheme from '../hooks/useColorScheme'
import { 
  apiUrl, 
  apiHeader,  
  getScrambledUri, 
  randomizeString ,
  connectionTabIndices
} from '../utils'
import { useNavigation } from '@react-navigation/native'
import FadeInOutView from './FadeInOutView'
import ProfileActionBar from './ProfileActionBar'
import { UserStateType } from '../types/userTypes'
import { ComponentInterface } from '../types/componentTypes'

const mapStateToProps = (state: { userReducer: UserStateType }) => {
  return {
    profile: state.userReducer.profile,
    initialTokens: state.userReducer.initialTokens
  }
}

const connector = connect(mapStateToProps)
type Props = ConnectedProps<typeof connector> & ComponentInterface['LocationListItem']

const LocationListItem = ({ location, profile, getLocations, initialTokens }: Props) => {
  const owner = location.item.owner
  const navigation = useNavigation()
  const defaultAvatar = "https://avatars0.githubusercontent.com/u/32242596?s=460&u=1ea285743fc4b083f95d6ee0be2e7bb8dcfc676e&v=4"

  const colorScheme = useColorScheme()
  const isBlocked = profile.blocked_profiles.includes(owner.id)
  const isVisible = profile.is_visible
  const avatarUri = _.get(owner, 'avatar', defaultAvatar) 
  const scrambledUri = getScrambledUri(owner)
  const [scrambledUsername, setScrambledUsername] = useState('')

  useEffect(() => {
    setScrambledUsername(randomizeString(owner.username))
  }, [isVisible])

  const handleOnPress = () =>{
    if(!isBlocked && isVisible) {
      navigation.navigate('ProfileDetail', { profile: owner })
    } else {
      navigation.navigate('ContextModal', { profile: owner })
    }
  }

  const handleOnDoublePress = () => {
    if(!isBlocked && isVisible) {
      makeConnection()
    } else {
      navigation.navigate('ContextModal', { profile: owner })
    }
  }

  const makeConnection = () => {
    const connection = owner.connection_to_user
    if (!connection) {
      const data = JSON.stringify({
        initiator_id: profile.id,
        responder_id: owner.id
      })
      axios.post(`${apiUrl}/connections/`, data, apiHeader(initialTokens.access_token)).then(() => {
        getLocations()
      }).catch(err => { console.log(JSON.stringify(err)) })
    } else {
      const isInitiator = connection.initiator.id === profile.id
      if (!isInitiator && !connection.accepted) {
        const data = JSON.stringify({
          accepted: true
        })
        axios.patch(`${apiUrl}/connections/${connection.id}/`, data, apiHeader(initialTokens.access_token)).then(() => {
          getLocations()
        }).catch(err => { console.log(JSON.stringify(err)) })
      }
    }
  }

  const wave = () => {
    const data = JSON.stringify({
      connection: owner.connection_to_user.id,
      sender_id: profile.id,
      text: 'hi'
    })

    axios.post(`${apiUrl}/messages/`, data, apiHeader(initialTokens.access_token)).then(() => {
      alert(`you waved to ${owner.username}`)
    }).catch(err => { console.log(JSON.stringify(err)) })
  }

  const handleAction = (action: string) => {
    if (!isVisible) {
      navigation.navigate('ContextModal', { profile: owner })
      return
    }

    switch(action) {
      case 'add-connection':
        makeConnection()
        break
      case 'pending':
        alert('pending')
        break
      case 'connected':
        alert('already connected')
        break
      case 'message':
        navigation.navigate('MessageListScreen', { connectionId: owner.connection_to_user.id })
        break
      case 'wave':
        wave()
        break
      case 'view-connections':
        navigation.navigate('ConnectionListScreen', { profile: owner, tabIndex: connectionTabIndices.all })
        break
    }
  }
  
  return (
    <DoublePressOpacity
      style={styles.container}
      onLongPress={() => makeConnection()}
      onPress={handleOnPress}
      onDoublePress={handleOnDoublePress}
    >
      <View>
        <View style={styles.contextMenu}>
          <View>
            { (!isVisible || isBlocked) &&
              <Ionicons
                name="eye-off-outline"
                size={22}
              />
            }
          </View>
          <TouchableOpacity 
            onPress={() => navigation.navigate('ContextModal', { profile: owner })}
          >
            <Ionicons
              name="ellipsis-horizontal"
              size={25}
              color={Colors[colorScheme].text}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.avatarContainer}>
        <Image
          style={styles.avatar}
          source={{
            uri: avatarUri
          }}
          blurRadius={0}
        />
        <FadeInOutView style={styles.fadeAvatar} isVisible={!isBlocked && isVisible}>
          <Image
            style={styles.avatar}
            source={{
              uri:scrambledUri
            }}
            blurRadius={10}
          />
        </FadeInOutView>
      </View>
      <View style={styles.userTitle}>
        <Text style={styles.userTitleText}>
          {isVisible ? owner.username : scrambledUsername} 
        </Text>
      </View>
      <ProfileActionBar 
        owner={owner} 
        profile={profile} 
        handleAction={handleAction} />
    </DoublePressOpacity>
  )
}

export default connector(LocationListItem)

const cardHeight = 382 //TODO: use computed heights adjacent Views
const windowWidth = Dimensions.get('window').width

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff', 
    height: cardHeight,
    borderRadius: 24,
    padding: 12,
    marginVertical: 24,
    justifyContent: 'space-between',
    shadowColor: '#333',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.1,
    shadowRadius: 4,
  },
  contextMenu: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 8,
    paddingVertical: 4,
  },
  userTitle: {
    padding: 16
  },
  userTitleText: {
    fontWeight: 'bold', 
    letterSpacing: 2,
    textAlign: 'center'
  },
  avatarContainer: {
    marginHorizontal: 32,
    borderRadius: windowWidth/2, 
    overflow: 'hidden'
  },
  avatar: {
    width: '100%',
    aspectRatio: 1
  },
  fadeAvatar: {
    position: 'absolute'
  },
  actionBar: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  actionBarIcon: {
    padding: 8,
  },
  userContent: {
    padding: 8,
    marginBottom: 16,
    textAlign: 'justify',
    borderBottomWidth: 1,
    height: 52
  },
  userName: {
    paddingBottom: 8,
    fontWeight: 'bold'
  },
  userBio: {
    paddingBottom: 8,
  },
  separator: {
    marginVertical: 0,
    height: 1,
    width: '100%',
  },
  blurView: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    height: '100%',
    width: '100%',
    backgroundColor: 'rgba(255,25,255,1)',
  }
})
