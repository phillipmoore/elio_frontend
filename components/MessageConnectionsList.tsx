import axios from 'axios'
import React from 'react'
import { FlatList, TouchableOpacity, StyleSheet } from 'react-native'
import { ListItem, Avatar} from 'react-native-elements'
import { useNavigation } from '@react-navigation/native'
import { Ionicons } from '@expo/vector-icons'

import { Text } from '../components/Themed'
import Dot from '../components/Dot'
import { apiUrl, apiHeader } from '../utils'

import { ComponentInterface } from '../types/componentTypes'
import { ConnectionInterface } from '../types/modelTypes'

const MessageConnectionsListItem = (props: ComponentInterface['MessageConnectionsListItem']) => {
  const { 
    initiator, 
    responder, 
    last_message, 
  } = props.connection.item
  const otherProfile = props.profile.id !== initiator.id ? initiator : responder
  const wasNotOpenedAndOtherProfile = (last_message &&
    !last_message?.was_opened && 
    last_message?.sender_id === otherProfile.id)
  const lastMessageKey = wasNotOpenedAndOtherProfile ? 'Unread' : '' 
  const iconColor = wasNotOpenedAndOtherProfile ? '#000' : '#333'
  const subTitleText = last_message?.text ? 
    `${last_message?.text.substring(0, 12)}...` : 
    '...'
  const navigation = useNavigation()

  const markMessageAsRead = (msgId: any) => {
    const data =JSON.stringify({
      was_opened: true
    })
    axios.patch(`${apiUrl}/messages/${msgId}/`, data, apiHeader(props.initialTokens.access_token))
    .then(() => {
      // mark last message as read on this component as well as messageListScreen 
      // so we can be sure it gets marked.  if user quickly navigates back,
      // then the components can be unmounted and not marked
      navigation.navigate('MessageListScreen', { connectionId: props.connection.item.id })
    })
    .catch(err => console.log(err))
  }

  return (
    <ListItem 
      Component={TouchableOpacity}
      key={`message_${props.connection.index}`}
      onPress={() => {
        markMessageAsRead(props.connection.item.last_message.id)}
      }
    >
      <Avatar 
        source={{uri: otherProfile.avatar}} 
        rounded={true}
        size={'large'}
      />
      <ListItem.Content style={styles.textSection}>
        <ListItem.Title>
          <Text style={styles[`titleText${lastMessageKey}`]}>{otherProfile.username}</Text>
        </ListItem.Title>
        <ListItem.Subtitle>
          <Text style={styles[`subtitleText${lastMessageKey}`]}>{subTitleText}</Text>
        </ListItem.Subtitle>
      </ListItem.Content>
      <ListItem.Content>
        {wasNotOpenedAndOtherProfile &&
          <Dot />
        }
      </ListItem.Content>
      <ListItem.Content style={styles.alignRight}>
        <Ionicons
          name="create-outline"
          size={25}
          color={iconColor}
        />
      </ListItem.Content>
    </ListItem>
  )
}



const MessageConnectionsList = ({ connections, profile, initialTokens }: ComponentInterface['MessageConnectionsList']) => {
  return (
    <FlatList 
      data={connections}
      renderItem={(connection: ConnectionInterface) => {
          return (
            <MessageConnectionsListItem
              connection={connection} 
              profile={profile} 
              initialTokens={initialTokens}
            />
          )
        }
      }
    />
  )
}


export default MessageConnectionsList

const styles = StyleSheet.create({
  textSection: {
    flex: 1,
  },
  titleText: {
    fontSize: 14,
  },
  subtitleText: {
    color: '#555',
    fontSize: 14,
  },
  titleTextUnread: {
    fontSize: 14,
    fontWeight: 'bold'
  },
  subtitleTextUnread: {
    color: '#000',
    fontSize: 14,
  },
  alignRight: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row'
  }
})