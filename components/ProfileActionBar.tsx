import * as React from 'react'
import { StyleSheet, TouchableOpacity } from "react-native"
import { Ionicons } from '@expo/vector-icons'
import { View } from './Themed'

import { ComponentInterface } from '../types/componentTypes'

const ProfileAcionBar = ({ owner, profile, handleAction }: ComponentInterface['ProfileAcionBar']) => {
  const textColor = '#000'
  return (
    <View style={styles.actionBar}>
      {(!owner.connection_to_user || (owner.connection_to_user && 
          owner.connection_to_user.initiator.id !== profile.id &&
          !owner.connection_to_user.accepted)) &&
        <TouchableOpacity
          style={styles.actionButton}
          onPress={() => handleAction('add-connection')}
        >
          <Ionicons 
            name="ios-heart-outline"
            size={24}
            color={textColor}
            style={styles.actionBarIcon}
          />
        </TouchableOpacity>
      }
      {(owner.connection_to_user && 
          owner.connection_to_user.initiator.id === profile.id &&
          !owner.connection_to_user.accepted) &&
        <TouchableOpacity
          style={styles.actionButton}
          onPress={() => handleAction('pending')}
        >
          <Ionicons 
            name="ios-heart"
            size={26}
            color={'crimson'}
            style={styles.actionBarIcon}
          />
        </TouchableOpacity>
      }
      {(owner.connection_to_user && 
        owner.connection_to_user.accepted) &&
        <View style={styles.actionButtonGroup}>
          <TouchableOpacity
            onPress={() => handleAction('connected')}
          >
            <Ionicons 
              name="ios-heart"
              size={26}
              color={'crimson'}
              style={styles.actionBarIcon}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => handleAction('message')}
          >
            <Ionicons
              name="ios-chatbubble-outline"
              size={22}
              color={textColor}
              style={styles.actionBarIcon}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => handleAction('wave')}
          >
            <Ionicons
              name="ios-hand-right-outline"
              size={23}
              color={textColor}
              style={styles.actionBarIcon}
            />
          </TouchableOpacity>
        </View>
      }
      <TouchableOpacity 
        style={styles.connectionCount}
        onPress={() => handleAction('view-connections')}
      >
        <Ionicons
          name="ios-people-outline"
          size={25}
          color={textColor}
          style={styles.actionBarIcon}
        />
      </TouchableOpacity>
    </View>
  )
}

export default ProfileAcionBar

const styles = StyleSheet.create({
  actionBar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 8,
    flex: 1
  },
  actionButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  actionButtonGroup: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  actionBarIcon: {
    paddingRight: 8,
  },
  actionButtonText: {
    letterSpacing: 4,
    color: '#000'
  },
  connectionCount: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  ccText: {
    fontSize: 16,
    paddingRight: 2,
  }
})
