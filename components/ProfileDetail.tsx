
import axios from 'axios'
import React, { useState, useEffect, ReactElement } from 'react'
import { 
  StyleSheet, 
  FlatList, 
} from 'react-native'
import { connect, ConnectedProps } from 'react-redux'
import { useHeaderHeight } from '@react-navigation/elements'
import { ListItem, Avatar } from 'react-native-elements'
import { NativeStackScreenProps } from '@react-navigation/native-stack'

import InstagramView from './profileComponents/InstagramView'
import { Text, View } from './Themed'
import { apiUrl, apiHeader, connectionTabIndices, PAD } from '../utils'
import ConnectionsItem from './profileComponents/ConnectionsItem'
import ContentListItem from './profileComponents/ContentListItem'
import ContentDetailContainer from './profileComponents/ContentDetailContainer'
import ContentTopDetail from './profileComponents/ContentTopDetail'
import { setHeaderHeight, setProfileContentHeight } from '../actions/animation'

import { RootStackParamList } from '../types'
import { ComponentInterface } from '../types/componentTypes'
import { 
  ResourceType, 
  WEB_PAGE_RESOURCE,
  INSTAGRAM_RESOURCE,
  YOUTUBE_RESOURCE 
} from '../types/modelTypes'

// TODO: use React.memo ---> const InstagramMemoView = React.memo(InstagramView)

const mapStateToProps = (state: any) => {
  return {
    animations: state.animationReducer,
    initialTokens: state.userReducer.initialTokens,
    topData: state.userReducer.topData
  }
}

const mapDispatchToProps =  {
  setHeaderHeight,
  setProfileContentHeight
}

const connector = connect(mapStateToProps, mapDispatchToProps)
type PropsFromRedux = ConnectedProps<typeof connector>
type OwnProps = NativeStackScreenProps<RootStackParamList, any>
type Props = PropsFromRedux & OwnProps & ComponentInterface['ProfileDetail']

const ProfileDetail = (
  { 
    navigation, 
    profile, 
    isLoggedIn, 
    setHeaderHeight, 
    setProfileContentHeight,
    initialTokens,
    topData
  }: Props) => {
  const appHeaderHeight = useHeaderHeight()
  const [TopComponents, setTopComponents] = useState<ReactElement[]>([])
  const [BottomComponents, setBottomComponents] = useState<ReactElement[]>([])

  const sourceBottomComponents = (resourceData: ResourceType[]) => {
    const components: any = []
    for (const index in resourceData) {
      const resource = resourceData[index]
      if (resource.resource_type === WEB_PAGE_RESOURCE) { // TODO: get tokens & URls & handles for each and pass as props
        components.push(<View key={`${index}_bot_comp`}><Text>Web Page</Text></View>)
      } else if (resource.resource_type === INSTAGRAM_RESOURCE) {
        components.push(<InstagramView key={`${index}_bot_comp`} />)
      } else if (resource.resource_type === YOUTUBE_RESOURCE) {
        components.push(<View key={`${index}_bot_comp`} />)
      } 
    }
    setBottomComponents(components) 
  }

  useEffect(() => {
    axios.get(`${apiUrl}/profile-resources/?profile_id=${profile.id}`, apiHeader(initialTokens.access_token)).then(res => {
      console.log(res.data)
      sourceBottomComponents(res.data.results)
    })
  }, [])

  useEffect(() => {
    const components: ReactElement[] = [] 
    for (const index in topData) {
      components.push(<ContentTopDetail key={`${index}_top_comp`} index={index} />)
    }
    setTopComponents(components)
  }, [topData])

  const measureHeader = (e: any) => {
    // adding the main header with profile header to get total height
    setHeaderHeight(appHeaderHeight + e.nativeEvent.layout.height)
  }

  interface ContentInterface {
    index: number
    item: ReactElement
  }
    
  return (
    <View style={styles.container}>
      <View onLayout={measureHeader} >
        <ListItem style={styles.profileHeader}>
          <Avatar 
            source={{uri: profile.avatar}} 
            rounded={true}
            size={'large'}
          />
          <ConnectionsItem 
            navigation={navigation} 
            profile={profile} 
            index={connectionTabIndices.all} 
          />
          {!isLoggedIn &&
            <ConnectionsItem 
              navigation={navigation} 
              profile={profile} 
              index={connectionTabIndices.mutual} 
            />
          }
        </ListItem>
        <View style={styles.handleBio}>
          <Text style={styles.handleText}>{profile.username}</Text>
          <Text style={styles.bioText}>{profile.bio}</Text>
        </View>
      </View>
      <View 
        style={styles.contentContainer}
        onLayout={e =>{
          setProfileContentHeight(e.nativeEvent.layout.height)
        }}
      > 
        <FlatList
          style={styles.contentList}
          data={BottomComponents}
          renderItem={(content: ContentInterface) => (
            <ContentListItem 
              key={`${content.index}_content_item`}
              content={content} 
              profile={profile}
            >
              {BottomComponents[content.index]}
            </ContentListItem>
          )
        }
          numColumns={2}
          windowSize={5}
        />
        <ContentDetailContainer
          ComponentToShow={BottomComponents} 
          profile={profile} 
          isTopLevel={false}
          testing={true}
        />
        <ContentDetailContainer
          ComponentToShow={TopComponents} 
          profile={profile} 
          isTopLevel={true}
          testing={true}
        />
      </View>
    </View>
  )
}

export default connector(ProfileDetail)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  profileHeader: {},
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  handleBio: {
    width: '100%',
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 16,
    borderBottomWidth: 0.25,
    borderBottomColor: '#e6e6e6',
  },
  handleText: {
    fontWeight: 'bold',
  },
  bioText: {
    textAlign: 'justify',
  },
  contentContainer: {
    overflow: 'hidden', 
    flex: 1,
  },
  contentList: {
    padding: PAD,
    flex: 1,
  },
})
