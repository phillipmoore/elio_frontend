import React, { useState } from 'react'
import {
  View,
  Dimensions,
  Animated,
  PanResponder,
  Easing,
  StyleSheet,
  PanResponderGestureState
} from 'react-native'
import * as Haptics from 'expo-haptics'

const SCREEN_WIDTH = Dimensions.get('window').width
const FORCING_DURATION = 350
const SCROLL_THRESHOLD = SCREEN_WIDTH / 30
const BUTTONS_THRESHOLD = SCREEN_WIDTH / 4


const Item = (props: any) => {
  const [scrollView, setScrollView] = useState(false)
  const LeftButton = props.leftButton
  const RightButton = props.rightButton
  const position = new Animated.ValueXY(0, 0)
  let crossedThresh = false

  const panResponder = PanResponder.create({
    onStartShouldSetPanResponder: () => false, // we don't want the item to be animated with a touch
    onMoveShouldSetPanResponder: () => true, // we want to animate the item with a movement
    onResponderTerminationRequest: () => false,
    onPanResponderGrant: () => {
      position.setValue({ x: 0, y: 0 }) // clearing the position
    },
    onPanResponderMove: (event, gesture) => {
      if (gesture.dx >= SCROLL_THRESHOLD) {
        enableScrollView(true)
        const x = gesture.dx - SCROLL_THRESHOLD
        position.setValue({ x, y: 0 })

      } else if (gesture.dx <= -SCROLL_THRESHOLD) {
        enableScrollView(true)
        const x = gesture.dx + SCROLL_THRESHOLD
        position.setValue({ x, y: 0 })
      }

      // Haptics
      if (gesture.dx > 0 && gesture.dx >= BUTTONS_THRESHOLD && !crossedThresh) {
        Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light)
        crossedThresh = true
      } else if ( gesture.dx > 0 && gesture.dx < BUTTONS_THRESHOLD && crossedThresh) {
        Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light)
        crossedThresh = false
      } else if (gesture.dx < 0 && gesture.dx <= -BUTTONS_THRESHOLD && !crossedThresh) {
        Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light)
        crossedThresh = true
      } else if (gesture.dx < 0 && gesture.dx > -BUTTONS_THRESHOLD && crossedThresh) {
        Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light)
        crossedThresh = false
      }
    },
    onPanResponderRelease: (event, gesture) => {
      position.flattenOffset() // adding animated value to the offset value then it reset the offset to 0.
      if (gesture.dx > 0) {
        userSwipedRight(gesture)
      } else {
        userSwipedLeft(gesture)
      }
    },
    onPanResponderTerminate: () => {
      Animated.spring(position, {
        toValue: { x: 0, y: 0 },
        useNativeDriver: true
      }).start()
    }
  })

  const getRightButtonProps = () => {

    const opacity = position.x.interpolate({
      inputRange: [-1, 0],
      outputRange: [1, 0]
    })
    const transform = [{scale: position.x.interpolate({
      inputRange: [-SCREEN_WIDTH, -BUTTONS_THRESHOLD, -BUTTONS_THRESHOLD, 0],
      outputRange: [1.5, 1, 0.4, 0]
    })}]
    return {
       opacity,
       transform
    }
  }

  const getLeftButtonProps = () => {
    const opacity = position.x.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1]
    })
    const transform = [{scale: position.x.interpolate({
      inputRange: [0, BUTTONS_THRESHOLD, BUTTONS_THRESHOLD, SCREEN_WIDTH],
      outputRange: [0, 0.4, 1, 1.6]
    })}]
    return {
      opacity,
      transform
    }
  }

  const resetPosition = () =>{
    Animated.timing(position, {
      toValue: { x: 0, y: 0 },
      duration: 200,
      useNativeDriver: true
    }).start(() => enableScrollView(false))
    crossedThresh = false
  }

  const completeSwipe = (callback: any) => {
    Animated.timing(position, {
      toValue: { x: 0, y: 0 },
      duration: FORCING_DURATION,
      useNativeDriver: true

    }).start()
    callback()
  }

  const enableScrollView = (isEnabled: boolean) => {
    if (scrollView !== isEnabled) {
      props.swipingCheck(isEnabled)
      setScrollView(isEnabled)
    }
  }

  const userSwipedLeft = (gesture: PanResponderGestureState) => {
    if (gesture.dx <= -(BUTTONS_THRESHOLD)) {
      completeSwipe(props.rightButtonPressed.bind(this))
      resetPosition()
    } else {
      resetPosition()
    }
  }

  const userSwipedRight = (gesture: PanResponderGestureState) => {
    if (gesture.dx >= BUTTONS_THRESHOLD) {
      completeSwipe(props.leftButtonPressed.bind(this))
      resetPosition()
    } else {
      resetPosition()
    }
  }

  const showButton = (side: string) => {
    const x = side === 'right' ? SCREEN_WIDTH / 4 : -SCREEN_WIDTH / 2.5
    Animated.timing(position, {
      toValue: { x, y: 0 },
      duration: 400,
      easing: Easing.out(Easing.quad),
      useNativeDriver: true
    }).start(() => enableScrollView(false))
  }

  const { containerStyle, leftButtonContainer, textContainer, rightButtonContainer } = styles
  return (
    <View style={containerStyle}>

      <Animated.View // LEFT BUTTON
        style={[leftButtonContainer, getLeftButtonProps()]}
      >
        <LeftButton />
      </Animated.View>

      <Animated.View // THE CONTENT OF ITEM
        style={[textContainer, {transform: position.getTranslateTransform()}]}
        {...panResponder.panHandlers}
      >
        {props.children}
      </Animated.View>

      <Animated.View // RIGHT BUTTON
        style={[rightButtonContainer, getRightButtonProps()]}
      >
        <RightButton />
      </Animated.View>
    </View>
  )
}

const styles = StyleSheet.create({
  leftButtonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    paddingHorizontal: 32,
    position: 'absolute',
    elevation: 3,
  },
  containerStyle: {
    flex: 1,
    flexDirection: 'row',
  },
  textContainer: {
    width: '100%',
    backgroundColor: '#CFD8DC',
    elevation: 3,
    zIndex: 2
  },
  textStyle: {
    fontSize: 17
  },
  rightButtonContainer: {
    position: 'absolute',
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    paddingHorizontal: 32,
    elevation: 3,
    zIndex: 1
  }
})

export default Item