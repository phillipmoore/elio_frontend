import * as React from 'react'
import { Pressable, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { View, TabBarIcon } from '../components/Themed'
import Dot from '../components/Dot'
import Beacon from '../components/Beacon'

const TabBarButton = (props: any) => {
  const iconColor = props.children.props.children[1].props.style[1].color
  const hasNotifications = props.notifications[props.sectionName]?.id_list.length > 0
  const choices: any = {
    locations: {
      iconName: "ios-home-outline"
    },
    messages: {
      iconName: "chatbubbles-outline"
    },
    connections: {
      iconName: "people"
    },
    profile: {
      iconName: "ios-person-circle-outline"
    }
  }

  return (
    <Pressable {...props} style={[props.style, styles.button]}>
      <TabBarIcon name={choices[props.sectionName].iconName} color={iconColor} />
      <View style={styles.badgeContainer}>
        { hasNotifications &&
          <View>
            {props.sectionName !== 'locations' ? <Dot/> : <Beacon/>}
          </View>
        }
        {/*<Text>{JSON.stringify()}</Text>*/}
      </View>
    </Pressable>
  )
}

const mapStateToProps = (state: any) => {
  return {
    notifications: state.userReducer.notifications
  }
}

export default connect(mapStateToProps)(TabBarButton)

const styles = StyleSheet.create({
  button: {
    // borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  badgeContainer: {
    height: 6,
    width: '100%',
    paddingTop: 6,
    alignItems: 'center'
  }
}) 



