import axios from 'axios'
import _ from 'lodash'
import * as React from 'react'
import { connect } from 'react-redux'
import * as Location from 'expo-location'
import * as TaskManager from 'expo-task-manager'

import { apiUrl, apiHeader } from '../utils'
import { setUserAndInitialTokens } from '../screens/AuthenticationScreen'
import { setUpdate, setNotifications, setUserLocation } from '../actions/user'

import store from '../store'


export const resetNotifications = (profileId: any, accessToken: any ) => {
  axios.get(`${apiUrl}/notifications/${profileId}/`, apiHeader(accessToken)).then(res => {
    store.dispatch(setNotifications(res.data))
  }).catch(err=> console.log(err) )
}

const LOCATION_TASK = 'background-location'

const TaskComponent = () => {
  React.useEffect(() => {
    (async () => {
      await 
      Location.startLocationUpdatesAsync(LOCATION_TASK, {
        accuracy: Location.Accuracy.Balanced
      })
    })()
  }, [])

  return null
}

let lastLatitude = 0
let lastLongitude = 0
let lastTimestamp = 0
let updateInterval = 0

TaskManager.defineTask(LOCATION_TASK, ({ data, error }) => {
  if (error) {
    console.log(error)
    return
  }

  lastLatitude = _.get(data, 'locations[0].coords.latitude')
  lastLongitude = _.get(data, 'locations[0].coords.longitude')  
 
  if (updateInterval < 90000) { // change to 10000
    const timePassed = _.get(data, 'locations[0].timestamp') - lastTimestamp
    updateInterval = updateInterval + timePassed
  } else {
    const state = store.getState()
    const props = _.get(state, 'userReducer')

    // update the apps current 'update', aka timestamp
    const updateTimestamp = new Date()
    store.dispatch(setUpdate(updateTimestamp))

    // update position in database of user every 10 seconds
    updateInterval = 0

    const positionData = {
      latitude: parseFloat(lastLatitude.toFixed(6)),
      longitude: parseFloat(lastLongitude.toFixed(6)), 
      location_time: new Date(_.get(data, 'locations[0].timestamp'))
    }

    // update user location in app for use with map
    store.dispatch(setUserLocation(positionData))

    axios.patch(`${apiUrl}/userlocation/${props.user.last_profile_id}/`, JSON.stringify(positionData), apiHeader(props.initialTokens.access_token)).catch((err) => {
      console.log(err)
      // if token has expired, log back in again
      if (err.response.status === 401) {
        setUserAndInitialTokens('refresh_token', props.user.email, '', props.initialTokens.refresh_token).then(userRes => {
          console.log('This is refreshing')
        }).catch(err=> {
          console.log('We were not able to log in again ', err)
        })
      }
    })

    // notifications
    resetNotifications(props.user.last_profile_id, props.initialTokens.access_token)
  }

  lastTimestamp = _.get(data, 'locations[0].timestamp')
})

export default TaskComponent
