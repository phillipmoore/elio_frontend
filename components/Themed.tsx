/**
 * Learn more about Light and Dark modes:
 * https://docs.expo.io/guides/color-schemes/
 */

import _ from 'lodash'
import { Ionicons } from '@expo/vector-icons';

import React, { useState } from 'react';
import { 
  Text as DefaultText, 
  View as DefaultView,
  TextInput as DefaultTextInput,
  TouchableOpacity,
  StyleSheet
} from 'react-native';

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';

export function useThemeColor(
  props: { light?: string; dark?: string },
  colorName: keyof typeof Colors.light & keyof typeof Colors.dark
) {
  const theme = useColorScheme();
  const colorFromProps = props[theme];

  if (colorFromProps) {
    return colorFromProps;
  } else {
    return Colors[theme][colorName];
  }
}

type ThemeProps = {
  lightColor?: string;
  darkColor?: string;
};

export type TextProps = ThemeProps & DefaultText['props'];
export type ViewProps = ThemeProps & DefaultView['props'];
// export type TextInputProps = ThemeProps & DefaultView['props'];

export function Text(props: TextProps) {
  const { style, lightColor, darkColor, ...otherProps } = props;
  const color = useThemeColor({ light: lightColor, dark: darkColor }, 'text');

  return <DefaultText style={[{ color }, style]} {...otherProps} />;
}

export function View(props: ViewProps) {
  const { style, lightColor, darkColor, ...otherProps } = props;
  const backgroundColor = useThemeColor({ light: lightColor, dark: darkColor }, 'background');

  return <DefaultView style={[{ backgroundColor }, style]} {...otherProps} />;
}

export const TextInput: any = React.forwardRef((props, ref) => {
  const theme = useColorScheme()
  const clonedProps = _.cloneDeep(props)
  clonedProps.style = {
    width: '100%',
    borderBottomWidth: 1,
    borderColor: Colors[theme].thinLine,
    padding: 16,
    paddingTop: 8,
    paddingBottom: 8,
    ...props.style
  }

  return <DefaultTextInput ref={ref} {...clonedProps} />
})

export const Button = (props: any) => {
  const theme = useColorScheme()
  const { onPress, title = props.children } = props;
  // const clonedProps = _.cloneDeep(props)
  const btnStyles = {
    backgroundColor: Colors[theme].buttonBackground,
    ...styles.button,
  }
  const btnTextStyles = {
    color: Colors[theme].text,
    ...styles.text
  }
  return (
    <TouchableOpacity style={btnStyles} onPress={onPress}>
      <Text style={btnStyles}>{title}</Text>
    </TouchableOpacity>
  );
}


export const ProfileItem = (profile: any) => (
  <View>
    <Text>{JSON.stringify(profile)}</Text>
    <Text>******</Text>
  </View>
);

type FieldProps = {
  name: string,
  value: string,
}

export const ProfileEditField = (props: FieldProps) => {
  const { name, value } = props
  const theme = useColorScheme()
  return ( 
    <View style={{ flexDirection: 'row', padding: 16}}>
      <View style={{ width: '20%' }}>
        <Text style={{ paddingTop: 8, paddingBottom: 8, paddingRight: 8 }}>
          { name }
        </Text>
      </View>
      <View style={{ 
          flex: 1,
          borderBottomWidth: 1,
          borderColor: Colors[theme].thinLine 
        }}
      >
        <Text style={{ paddingTop: 8, paddingBottom: 8 }}>
          { value } 
        </Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 6,
    paddingHorizontal: 16,
    borderRadius: 8,
    elevation: 3,
  },
  text: {
    fontSize: 14,
    letterSpacing: 0.5,
  },
});

export const DoublePressOpacity = (props: any) => {
  const [timeout, storeTimout] = useState(false)
  const handleOnPress = () => {
    if (!!props.onDoublePress) {
      if (timeout) {
        clearTimeout(timeout)
        storeTimout(false)
        props.onDoublePress()
      } else {
        const to = setTimeout(() => {
          storeTimout(false)
          props.onPress()
        }, 250)
        storeTimout(to)
      }
    } else {
      if (props.onPress){
        props.onPress()
      }
    }
  }

  return ( 
    <TouchableOpacity {...props} onPress={(e: any) => handleOnPress(e)}>
      {props.children}
    </TouchableOpacity>
  )
}

export function TabBarIcon(props: {
  name: React.ComponentProps<typeof Ionicons>['name']
  color: string
}) {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />
}
