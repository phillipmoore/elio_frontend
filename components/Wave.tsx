import React, { useEffect, useRef } from 'react';
import { Animated, StyleSheet } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { View } from '../components/Themed';


const Wave = (props: any) => {
  const pulse = useRef(new Animated.Value(0)).current

  useEffect(() => {
    startPulse()
  }, [])

  const getRandomInt = (max: number) => {
    return Math.floor(Math.random() * max);
  }

  const startPulse = () => {
    Animated.sequence([
      Animated.delay(getRandomInt(6000)),
      Animated.timing(pulse, {
        toValue: 100,
        duration: 150,
        useNativeDriver: true
      }),
      Animated.timing(pulse, {
        toValue: -100,
        duration: 150,
        useNativeDriver: true
      }),
      Animated.timing(pulse, {
        toValue: 100,
        duration: 150,
        useNativeDriver: true
      }),
      Animated.timing(pulse, {
        toValue: 0,
        duration: 150,
        useNativeDriver: true
      }),
    ]).start(({finished}) => {
      if (finished) {
        startPulse()
      }
    })
  }

  const getPulseStyle = () => {
    const deg = pulse.interpolate({
      inputRange: [0, 100],
      outputRange: [0, 0.2]
    })
    
    return { transform: [{ rotateZ: deg }] }
  }

  return <View style={styles.container}>
    <Animated.View pointerEvents="none" style={[styles.main, getPulseStyle()]} >
      <Ionicons
        name={`ios-hand-${props.direction}-outline`}
        size={34}
        color={'#333'}
      />
    </Animated.View>
  </View>
}

export default Wave

const styles = StyleSheet.create({
  container: {
    width: 56,
    alignItems: 'center'
  },
  main: {}
})