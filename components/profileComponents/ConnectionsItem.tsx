import React from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import { ListItem } from 'react-native-elements'
import { Text } from '../../components/Themed'
import { connectionTabIndices } from '../../utils'

const ConnectionsItem = ({ navigation, profile, index }: any) => {
  const isAllConnections = index === connectionTabIndices.all
  const type = isAllConnections ? 'Connections' : 'Mutual'
  const typeKey = isAllConnections ? 'connection_count': 'mutual_count'
  return (
    <ListItem.Content style={styles.profileContent}>
      <TouchableOpacity
        onPress={() => navigation.push('ConnectionListScreen', 
          { profile: profile, tabIndex: index }
        )}
      >
        <ListItem.Title style={styles.profileContentTitle}>
          <Text style={styles.profileContentTitleText}>{profile[typeKey]}</Text>
        </ListItem.Title>
        <ListItem.Subtitle style={styles.profileContentSubTitle}>
          <Text>{type}</Text>
        </ListItem.Subtitle>
      </TouchableOpacity>
    </ListItem.Content>
  )
}

export default ConnectionsItem

const styles = StyleSheet.create({
  profileContent: {
    alignItems: 'center',
  },
  profileContentTitle: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  profileContentTitleText: {
    width: '100%',
    textAlign: 'center',
  },
  profileContentSubTitle: {
  },
})