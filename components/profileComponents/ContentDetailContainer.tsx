import React, { useState, useEffect } from 'react'
import { 
  StyleSheet, 
  Dimensions, 
  TouchableOpacity, 
  // Animated, 
  PanResponder
} from 'react-native'
import * as Haptics from 'expo-haptics'
import { connect } from 'react-redux'
import MaskedView from '@react-native-masked-view/masked-view'
import Animated, {
  withDelay,
  withSpring,
  withTiming,
  useAnimatedStyle,
  useSharedValue,
  interpolate,
  runOnJS
} from 'react-native-reanimated'
import { Text, View } from '../../components/Themed'
import { PAD, RADIUS } from '../../utils'
import { 
  setBottomScreenCoords, 
  setTopScreenCoords, 
  setBottomContainerOpen, 
  setTopContainerOpen,
  setScaleBottom,
  setScaleTop
} from '../../actions/animation'

const screenWidth = Dimensions.get('window').width

const ContentDetailContainer = (
  { 
    ComponentToShow, 
    profile, 
    isTopLevel,
    testing, 
    animations,
    setBottomScreenCoords,
    setTopScreenCoords,
    setBottomContainerOpen,
    setTopContainerOpen,
    setScaleTop,
    setScaleBottom
  }: any) => {

  const setScreenCoords = !isTopLevel ? setBottomScreenCoords : setTopScreenCoords
  const setContainerOpen = !isTopLevel ? setBottomContainerOpen : setTopContainerOpen
  const screenCoords = !isTopLevel ? animations.bottomScreenCoords : animations.topScreenCoords
  const containerIsOpen = !isTopLevel ? animations.bottomContainerOpen : animations.topContainerOpen
  const setScaleValue = !isTopLevel ? setScaleBottom : setScaleTop

  const scale = useSharedValue(0)
  const positionX = useSharedValue(0)
  const positionY = useSharedValue(0)
  const opacity = useSharedValue(0)
  const misc = useSharedValue(0)

  const [componentIndex, setComponentIndex] = useState(0)
  const [aspectRatio, setAspectRatio] =  useState(1)
  const [screen2ItemRatio, setScreen2ItemRatio] = useState(2)

  useEffect(() => {
    setScaleValue(scale)
    const isEmpty = Object.keys(screenCoords).length === 0

    if(!isEmpty) {
      openContainer(
        screenCoords.index, 
        screenCoords.x, 
        screenCoords.y, 
        screenCoords.aspectRatio,
        screenCoords.s2IRatio 
      )
    }
  }, [screenCoords] )

  const animatedContainerStyles = useAnimatedStyle(() => {
    return {
      transform: [
        {scaleX: interpolate(scale.value, [0, 1], [1/screen2ItemRatio, 1])}, 
        {scaleY: interpolate(scale.value, [0, 1], [1/screen2ItemRatio, 1])},
        {translateX: positionX.value},
        {translateY: positionY.value},
      ],
      opacity: opacity.value
    }
  })

  const animatedBackdropStyles = useAnimatedStyle(() => {
    return {
      opacity: interpolate(scale.value, [0, 0.9, 1], [0, 1, 1])
    }
  })

  const maskOpacityStyles = useAnimatedStyle(() => {
    return {
      opacity: misc.value
    }
  })

  const openContainer = (
      index: number, 
      x: number, 
      y: number, 
      aspectRatio: number, 
      s2IRatio: number
    ) => {
    console.log(ComponentToShow)
    opacity.value = 0
    setAspectRatio(aspectRatio)
    setScreen2ItemRatio(s2IRatio)
    positionX.value = x
    positionY.value = y
    setContainerOpen(true)
    setComponentIndex(index)

    if (testing) {
      opacity.value = withDelay(200, withTiming(1, { duration: 100 }, () => {
        // after opacity is changed
        scale.value = withTiming(1, { duration: 100 })
        misc.value = withTiming(1, { duration: 100 })
        positionX.value = withTiming(0, { duration: 100 })
        positionY.value = withTiming(0, { duration: 100 })
      }))
    } else {
      opacity.value = withTiming(1, { duration: 100 })
    }
  }

  const onClosedCallback = () => {
    const current = animations.panResponderParams?.ref?.current
    if (current) {
      current.scrollTo({ count: -current.getCurrentIndex()})
      setCurrentCarouselIndex(0)
    }
  }

  const closeContainer = () => {
    const x = screenCoords.x
    const y = screenCoords.y
    
    setScreenCoords({})
    setContainerOpen(false)

    const timingOptions = { duration: 150 }
    scale.value = withTiming(0, timingOptions)
    misc.value = withTiming(0, timingOptions)
    positionX.value = withTiming(x, timingOptions)
    positionY.value = withTiming(y, timingOptions, () => {
      opacity.value = withTiming(0, timingOptions, (finished) => {
        if (finished) {
          runOnJS(onClosedCallback)() 
        }
      })
    })
  }

  // ---- below begins the pan gesture close capabilities ----//

  const [scrollView, setScrollView] = useState(false)
  const [hasPanned, setHasPanned] = useState(true)
  const [currentCarouselIndex, setCurrentCarouselIndex] = useState(0)
  let crossedThresh = false

  const SCREEN_WIDTH = Dimensions.get('window').width
  const RIGHT_BUTTON_THRESHOLD = SCREEN_WIDTH / 15
  const FORCE_TO_OPEN_THRESHOLD = SCREEN_WIDTH / 3.5
  const FORCING_DURATION = 350
  const SCROLL_THRESHOLD = SCREEN_WIDTH / 20
  const CLOSE_THRESHOLD = SCREEN_WIDTH / 3

  const scaleContainer = (gesture) => {
    const absoluteDX = Math.abs(gesture.dx)
    if (absoluteDX >= SCROLL_THRESHOLD) {
      enableScrollView(true)
      const rv = 1 - ((absoluteDX-SCROLL_THRESHOLD)/SCREEN_WIDTH*2) 
      if (rv > 0.6) {
        scale.value = rv
      } else {
        scale.value = 0.4
      }  
    } 

    // Haptics
    if (absoluteDX > 0 && absoluteDX >= CLOSE_THRESHOLD && !crossedThresh) {
      Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light)
      crossedThresh = true
    } else if ( absoluteDX > 0 && gesture.dx < CLOSE_THRESHOLD && crossedThresh) {
      Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light)
      crossedThresh = false
    }
  }

  const panResponder = PanResponder.create({
    onStartShouldSetPanResponder: () => false, // we don't want the item to be animated with a touch
    onMoveShouldSetPanResponder: (evt, gesture) => {
      return (Math.abs(gesture?.dy) < Math.abs(gesture?.dx)) // should set on horizontal movement only 
    },
    onPanResponderTerminationRequest: () => true,
    onPanResponderMove: (event, gesture) => {
      const current = animations.panResponderParams.ref?.current
      const lastIndex = animations.panResponderParams.lastIndex
      const carouselTouched = animations.panResponderParams.touched
      const overScrollLeft = currentCarouselIndex === 0 && gesture.dx > 0 
      const overScrollRight = currentCarouselIndex === lastIndex && gesture.dx < 0 

      if (carouselTouched && !(overScrollLeft || overScrollRight)) {
        if (!hasPanned) {
          const direction = gesture.dx < 0 ? 1 : -1
          current.scrollTo({
            count: direction, 
            animated: true, 
            onFinished:() => setCurrentCarouselIndex(current?.getCurrentIndex())
          })
          setHasPanned(true)
        }
      } else {
        scaleContainer(gesture)
      } 
    },
    onPanResponderRelease: (event, gesture) => {
      const _closeOrReset = () => {
        const rv = 1 - ((Math.abs(gesture.dx)-SCROLL_THRESHOLD)/SCREEN_WIDTH*2)
        if (rv < 0.6) {
          closeContainer()
        } else {
          resetScale()
        }
      } 

      if (!animations.panResponderParams.touched) {
        _closeOrReset()
      } else {
        if (currentCarouselIndex === 0 || currentCarouselIndex === animations.panResponderParams.lastIndex) {
          _closeOrReset()
        }
      }

      setHasPanned(false)
    },
  })

  const resetScale = () =>{
    scale.value = withSpring(1)
    crossedThresh = false
  }

  const enableScrollView = (isEnabled) => {
    if (scrollView !== isEnabled) {
      setScrollView(isEnabled)
    }
  }

  return (
    <View
      style={styles.contentDetailContainer}
      pointerEvents={containerIsOpen ? 'auto': 'none'}
      {...panResponder.panHandlers}
    >
      <Animated.View 
        style={[
          styles.contentDetailContainer,
          animatedContainerStyles,
          { aspectRatio: aspectRatio },
        ]}
      >
        <MaskedView
          style={{ height: '100%',}}
          maskElement={
            <View
              style={{ backgroundColor: 'transparent', flex: 1}}
            >
              <Animated.View style={{ 
                  position: 'absolute',
                  width: '100%',
                  aspectRatio: aspectRatio,
                  backgroundColor: 'black', 
                  borderRadius: (screen2ItemRatio * RADIUS),

                }} 
              />
              <Animated.View style={[
                  { 
                    position: 'absolute',
                    height: '100%',
                    width: '100%',
                    backgroundColor: 'black',
                  },
                  maskOpacityStyles
                ]} 
              />
            </View>           
          }
        >
          <View style={styles.contentDetail}>
            {ComponentToShow[componentIndex]}
            {/*<TouchableOpacity style={{position: 'absolute', right: 0, zIndex: 400, backgroundColor: '#fff'}} onPress={() => closeContainer()}>
              <Text>click to close
              </Text>
            </TouchableOpacity>*/}
          </View>
        </MaskedView>
      </Animated.View>
      <Animated.View 
        style={[
          styles.backdrop,
          animatedBackdropStyles
        ]}
      />
    </View>
  )
}

const mapStateToProps = (state: any) => {
  return {
    animations: state.animationReducer
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    setBottomScreenCoords: (height: number) => dispatch(setBottomScreenCoords(height)),
    setTopScreenCoords: (height: number) => dispatch(setTopScreenCoords(height)),
    setBottomContainerOpen: (bool: boolean) => dispatch(setBottomContainerOpen(bool)),
    setTopContainerOpen: (bool: boolean) => dispatch(setTopContainerOpen(bool)),
    setScaleTop: (value: boolean) => dispatch(setScaleTop(value)),
    setScaleBottom: (value: boolean) => dispatch(setScaleBottom(value)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContentDetailContainer)

const styles = StyleSheet.create({
  contentDetailContainer: {
    position: 'absolute', 
    flex: 1,
    height: '100%',
    width: screenWidth,
    backgroundColor: 'transparent'
  },
  contentDetail: {
    overflow: 'hidden',
    height: '100%',
    backgroundColor: 'transparent',
    borderRadius: RADIUS
  },
  backdrop: {
    position: 'absolute',
    backgroundColor: 'rgba(255,255,255,0.8)',
    height: '100%',
    width: '100%',
    zIndex: -1
  }
})