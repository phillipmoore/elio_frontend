import React, { useState } from 'react';
import { 
  StyleSheet, 
  Dimensions, 
  TouchableOpacity, 
} from 'react-native'
// import { connect } from 'react-redux'
import { View } from '../../components/Themed';
import { setBottomContainerOpen } from '../../actions/animation'
import { expandToItem, PAD, RADIUS } from '../../utils'

const screenWidth = Dimensions.get('window').width

const ContentListItem = ({ content, profile, children, headerHeight, contentHeight }: any) => {
  const [itemLayout, setItemLayout] = useState({})
  const isTopLevel = false
  return (
    <View style={styles.itemContainer} key={`${content.index}_item`}>
      <TouchableOpacity 
        style={styles.item}
        onPress={e => {
          expandToItem(e, content.index, itemLayout, isTopLevel)
        }}
        onLayout={e => setItemLayout(e.nativeEvent.layout)}
      >
        <View style={styles.itemContent} pointerEvents={'none'} >
          {children}
        </View>
      </TouchableOpacity>
    </View>
  )
}
// const mapStateToProps = (state: any) => {
//   return {


//   }
// }

// const mapDispatchToProps = (dispatch: any) => {
//   return {
//     setBottomContainerOpen: (bool: boolean) => dispatch(setBottomContainerOpen(bool))
//   }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(ContentListItem)
export default ContentListItem

const styles = StyleSheet.create({
  itemContainer: {
    width: '50%',
    aspectRatio: 1,
    padding: PAD,
  },
  item: {
    backgroundColor: '#fff',
    borderRadius: RADIUS,
    overflow: 'hidden',
    flex: 1,
  },
  itemContent: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.1)',
  },
})