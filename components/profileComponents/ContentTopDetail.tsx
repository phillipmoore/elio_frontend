import React, { useState, useEffect, useRef } from 'react'
import { 
  // Animated, 
  Dimensions, 
  StyleSheet, 
  Image, 
  PanResponder, 
  Pressable,
  ScrollView 
} from 'react-native'
import { connect } from 'react-redux'
import Animated, {
  call,
  useCode,
  withTiming,
  useAnimatedStyle,
  interpolate
} from 'react-native-reanimated'
import Carousel from 'react-native-reanimated-carousel'
import { Video, AVPlaybackStatus } from 'expo-av'

import { Text, View } from '../../components/Themed'
import SwipeView from '../../components/SwipeView'
import { getVisibleIGSource, getShrunkenImage, PAD, RADIUS } from '../../utils'
import { getIgMediaData } from '../../resourceUtils'
import { setPanResponderParams } from '../../actions/animation'


const screenWidth = Dimensions.get('screen').width

const VideoPlayer = ({ item, topContainerOpen }: any) => {
  const video = useRef(null)
  const [status, setStatus] = useState({})
  const [isReady, setIsReady] = useState(false)
  const videoUri = item.media_url // TODO: add default video e.g 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4'
  const imageUri = getVisibleIGSource(item) // TODO: add default thumbnail 

  useEffect(() => {
    if (video) {
      if (!topContainerOpen) {
        video?.current.pauseAsync()
      } else {
        video?.current.playAsync()
      }
    }
  }, [topContainerOpen])

  return (
    <View style={[styles.resourceContainer, {aspectRatio: item.aspectRatio}]}>
      <Video
        ref={video}
        style={styles.resource}
        source={{
          uri: item ? item.media_url : 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
        }}
        useNativeControls={false}
        resizeMode="cover"
        isLooping
        onPlaybackStatusUpdate={status => setStatus(() => status)}
        onReadyForDisplay={(e: any) => setIsReady(true)}
        shouldPlay={true}
      /> 
      { !isReady &&
        <Image
          style={styles.resource}
          source={{ uri: imageUri }}
          resizeMode={'cover'}
        />
      }
    </View>
  )
}

const CarouselItem = ({ child }: any) => {
  const source = getShrunkenImage(getVisibleIGSource(child.item), 600)
  return (
    <View style={[styles.resourceContainer, {aspectRatio: child.item.aspectRatio}]}>
      <Image
        style={styles.resource}
        source={{ uri: source }}
        resizeMode={'cover'}
      />
    </View>
  )
}

const ContentTopDetail = (
  { data,
    index,
    aspectRatio, 
    instagramObject, 
    igResource, 
    topContainerOpen, 
    setPanResponderParams, 
    scaleTop,
    profileContentHeight 
  }: any) => {
  const [savedAspectRatio, setSavedAspectRatio] = useState(1)
  const [children, setChildren] = useState([])
  const item = data[index]
  const [translateOffset, setTranslateOffset] = useState(10)
  const [backgroundOpacity, setBackgroundOpacity] = useState(0)
  const carouselRef = useRef(null)
  const emptyRef = useRef(null)
  const [containerOpacity, setContainerOpacity] = useState(false)
  const [scrollIndex, setScrollIndex] = useState(0)
  const [reachedCloseThreshold, setReachedCloseThreshold] = useState(false)
  const [firstTouch, setFirstTouch] = useState(-1)
  const [secondTouch, setSecondTouch] = useState(-1)
  const [swipeDirection, setSwipeDirection] = useState('')
  const isOverScrollingLeft = (scrollIndex === 0 && swipeDirection === 'right')
  const isOverScrollingRight = (scrollIndex === children.length - 1 && swipeDirection === 'left')

  const getChildren = async (children: any) => {
    const tempChildren: any = []
    for (const index in children) {
      const id = children[index].id
      const response = await getIgMediaData(id, 'id,media_url,media_type')
      tempChildren.push(response.data)
    }
    if(tempChildren.length > 0) {
      setChildren(tempChildren)
    }
  }

  useEffect(() => {
    setSavedAspectRatio(aspectRatio)
    if (!!item?.media_type && item.media_type === 'CAROUSEL_ALBUM' && children.length <= 1) {
      setChildren([item]) // default image
      getIgMediaData(item.id, 'children').then((res: any) => {
        getChildren(res.data.children.data)
      })
    }

  }, [])

  const animatedContainerStyles = useAnimatedStyle(() => {
    return {
      transform: [
        {translateY: interpolate(scaleTop.value, [0, 1], [0, translateOffset])}
      ],
    }
  })

  const animatedBkgStyles = useAnimatedStyle(() => {
    const bkgOpacity = scaleTop.value > 0.5 ? scaleTop.value : 0
    return {
      backgroundColor: `rgba(255,255,255,${bkgOpacity})`
    }
  })

  const animatedSubContainerStyles = useAnimatedStyle(() => {
    return {
      transform: [
        {translateY: interpolate(scaleTop.value, [0, 1], [-(3 * PAD), 0])}, 
        {scaleX: interpolate(scaleTop.value, [0, 1], [1.1, 1])},
        {scaleY: interpolate(scaleTop.value, [0, 1], [1.1, 1])},
      ]
    }
  })

  const animatedCaptionStyles = useAnimatedStyle(() => {
    return {
      opacity: interpolate(scaleTop.value, [0, 0.5, 1], [0, 0, 1])
    }
  })


  return (
    <Animated.View 
      style={[
        styles.container,
        animatedContainerStyles,
        animatedBkgStyles
      ]}

      onLayout={(event) => {
        // KEEP AN EYE OUT. this function might be too slow
        // setting the end y coord for this View to appear in the middle of screen
        const offset = (profileContentHeight - event.nativeEvent.layout.height)/3
        setTranslateOffset(offset)
      }}
    >
      <Animated.View
        style={animatedSubContainerStyles}
      >
        <ScrollView style={styles.scrollView}>
          { !!item?.media_type && item.media_type !== 'CAROUSEL_ALBUM' ?
            ( item.media_type !== 'IMAGE' ?
              <VideoPlayer item={item} topContainerOpen={topContainerOpen} /> :
              <View style={[styles.resourceContainer, {aspectRatio: item.aspectRatio}]}>
                <Image
                  style={styles.resource}
                  source={{ uri: getVisibleIGSource(item) }}
                  resizeMode={'cover'}
                />
              </View>
            ) : 
            <View
              onTouchStart={(e) => {
                setPanResponderParams({ ref: carouselRef, lastIndex: children.length - 1, touched: true })
              }}
              onTouchEnd={() => {
                setPanResponderParams({ ref: carouselRef, lastIndex: 0, touched: false })
              }}
              style={[{aspectRatio: item.aspectRatio}, styles.bkgTransparent]}
            >

              <View 
                // this carousel is triggered by the panResponder on a layer above
                // see ContentDetailContainer -> panResponder
                pointerEvents={'none'}
                style={styles.bkgTransparent}
              >
                <Carousel
                  ref={carouselRef}
                  data={children}
                  renderItem={(child) => <CarouselItem child={child} />}
                  width={screenWidth}
                  loop={false}
                  mode={'parallax'}
                  modeConfig={{
                    parallaxScrollingOffset: 48,
                    parallaxScrollingScale: 1,
                    parallaxAdjacentItemScale: 0.8
                  }}            
                />
              </View>
            </View>
          }

            <Animated.View
              style={[
                styles.caption,
                animatedCaptionStyles
              ]}
            >
              <Text style={styles.captionText}>
                {item?.caption}
              </Text>
            </Animated.View>
        </ScrollView>
      </Animated.View>
    </Animated.View>
  ) 
}

const mapDispatchToProps = (dispatch: any ) => {
  return {
    setPanResponderParams: (obj: any) => dispatch(setPanResponderParams(obj))
  }
}

const mapStateToProps = (state: any) => {
  return {
    data: state.userReducer.topData,
    aspectRatio: state.animationReducer.topScreenCoords.aspectRatio,
    instagramObject: state.userReducer.instagramObject,
    igResource: state.resourceReducer.igResource,
    topContainerOpen: state.animationReducer.topContainerOpen,
    scaleTop: state.animationReducer.scaleTop,
    profileContentHeight: state.animationReducer.profileContentHeight
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContentTopDetail)

const styles = StyleSheet.create({
  bkgTransparent: {
    backgroundColor: 'transparent'
  },
  container: {
    width: '100%',
    borderRadius: RADIUS/2,
    backgroundColor: '#fff',
  },
  scrollView: {
    paddingTop: 3*PAD,
  },
  resourceContainer: {
    paddingHorizontal: 12,
    overflow: 'hidden',
    backgroundColor: 'transparent'
  },
  resource: {
    width: '100%',
    height: '100%',
    alignContent: 'center',
    borderRadius: RADIUS/4,
  },
  caption: {
  },
  captionText: {
    paddingHorizontal: 2*PAD,
    textAlign: 'justify',
    paddingTop: PAD, 
    paddingBottom: 2*PAD
  },
})