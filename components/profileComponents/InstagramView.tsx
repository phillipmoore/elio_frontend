import * as WebBrowser from 'expo-web-browser';
import axios from 'axios';
import _ from 'lodash'
import React, { useState, useEffect, useRef } from 'react';
import { 
  // Animated,
  StyleSheet, 
  TouchableOpacity, 
  FlatList, 
  Image, 
  Dimensions,
  LayoutAnimation,
  Platform,
  UIManager 
} from 'react-native';
import  { connect } from 'react-redux'
import Animated, {
  withTiming,
  withSpring,
  useAnimatedStyle,
  interpolate,
  runOnJS
} from 'react-native-reanimated'
import MasonryList from '@react-native-seoul/masonry-list'
import { useNavigation } from '@react-navigation/native'
import FastImage from 'expo-fast-image'
import { Text, View, TextInput } from '../../components/Themed'
import { 
  apiUrl,
  apiHeader,
  getShrunkenImage, 
  randomInt, 
  expandToItem, 
  getVisibleIGSource,
  PAD, 
  RADIUS 
} from '../../utils'
import { getIgMedia } from '../../resourceUtils'
import Beacon from './../Beacon'
import { setTopData } from '../../actions/user'
import { RootStackScreenProps } from '../../types';

const screenWidth = Dimensions.get('window').width

const InstagramListItem = (
  { 
    ig, 
    containerOpen, 
    columnNumber, 
    selectedIndex
  }: any) => {

  const source = getVisibleIGSource(ig.item)
  const baseWidth = Math.round(screenWidth/1.5) // 2 is sufficient, but 1.5 is a little sharper
  const minimizedSource = source //getShrunkenImage(source, baseWidth)
  // const imageRef = useRef(null)
  const [halfClosed, setHalfClosed] = useState(true)
  const [itemLayout, setItemLayout] = useState({})
  // const [imageLoaded, setImageLoaded] = useState(false)

  useEffect(() => {
    console.log()
    if (containerOpen ) { // selectedIndex replace 0 with index of where instagram is placed in the list, TBD
      setHalfClosed(false)
    } else {
      setHalfClosed(true)
    }
  },[containerOpen])

  const isTopLevel = true
  return (
    <TouchableOpacity 
      style={[styles.pressable, {aspectRatio: !halfClosed ? ig.item.aspectRatio : 1}]}
      onPress={(e) => expandToItem(e, ig.i, itemLayout, isTopLevel)}
      onLayout={(e) => {
        setItemLayout(e.nativeEvent.layout)
      }}
    >
      <FastImage
        // ref={imageRef}
        style={[styles.thumb, {aspectRatio: !halfClosed ? ig.item.aspectRatio : 1}]}
        source={{ uri: minimizedSource }}
        // resizeMode={'cover'} //!halfClosed ? 'contain' : 'cover'
        // onLoadStart={() => setImageLoaded(false)}
        // onLoadEnd={() => setImageLoaded(true)}
      />
    </TouchableOpacity>
  )
}

const InstagramView = (
  { 
    profile, 
    initialTokens,
    containerOpen, 
    selectedIndex, 
    setTopData, 
    igItems,
    scaleBottom 
  }: any ) => {

  const [tempPositionList, setTempPositionList] = useState([])
  const [imageHeight, setImageHeight] = useState(0)
  const [coumnNumber, setColumnNumber] = useState(2)
  const [halfClosed, setHalfClosed] = useState({})
  const listRef = useRef()



// const { width, height } = yield getImageSize;

  const getImageRatios = async (data: any) => {

    
    for (const index in data) {
      const src = getVisibleIGSource(data[index])
      const _getImageSize: any = new Promise (
        (resolve, reject) => {
          Image.getSize(src, (width, height) => {
            const aspectRatio = width/height
            resolve({ aspectRatio })
          })
        }, (error) => reject(error)
      )

      const { aspectRatio } = await _getImageSize

      data[index]['aspectRatio'] = aspectRatio
    }    
    setTopData(data)
  }

  useEffect(() => {
    // loading instagram media objects
    console.log('in the gram', selectedIndex, profile)
    getIgMedia(12).then((res: any) => { 
      console.log(res)
      // setTopData(res.data)

      getImageRatios(res.data)
    })
  },[])

  const paddingStyles = useAnimatedStyle(() => {
    return {
      padding: interpolate(scaleBottom.value, [0, 0.5, 1], [0, 0, 16])
    }
  })

  useEffect(() => {
    if (!containerOpen) {
      listRef?.current?.scrollTo({ x: 0, y: 0, animated: true })
      // console.log('reset this later')
    }
  }, [containerOpen])

  // const handleItemPosition = (heightObj: object) => {
  //   const objInList = tempPositionList.some(obj => obj.index === heightObj.index)
  //   if (!objInList) {
  //     tempPositionList.push(heightObj)
  //     setTempPositionList(tempPositionList)
  //     if(tempPositionList.length === igItems.length) {
  //       const sortedList = _.sortBy( tempPositionList, 'index')
  //       const mappedList = sortedList.map((obj: object) => {
  //         const firstHeightEntries = sortedList.slice(0, obj.index).map((item: object) => item.height)
  //         const position = firstHeightEntries.length !== 0 ? firstHeightEntries.reduce((a: number, b: number) => a + b) : 0
  //         return position
  //       })
  //       setItemPositionList(mappedList)
  //       setTempPositionList([])
  //     }
  //   }
  // }

  return (
    <Animated.View 
      style={[
        styles.list,
        paddingStyles
      ]}>
      <MasonryList 
        // style={[styles.list, paddingStyles]}
        // contentContainerStyle={[paddingStyles]}
        data={igItems}
        renderItem={ (ig: any) => 
          <InstagramListItem 
            key={`${ig.i}_instagram}`}
            ig={ig}
            containerOpen={containerOpen}
            selectedIndex={selectedIndex}
          />
        }
        numColumns={2}
        scrollEnabled={true}
        innerRef={listRef}
        decelerationRate="fast"
      />
    </Animated.View>
  );
}

const mapStateToProps = (state: any) => {
  return {
    profile: state.userReducer.profile,
    selectedIndex: state.animationReducer.bottomScreenCoords?.index,
    containerOpen: state.animationReducer.bottomContainerOpen,
    initialTokens: state.userReducer.initialTokens,
    igItems: state.userReducer.topData,
    scaleBottom: state.animationReducer.scaleBottom,
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    setTopData: (data: any) => dispatch(setTopData(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(InstagramView)

const styles = StyleSheet.create({
  list: {
    flex: 1,
    margin: -PAD/2
  },
  pressable: {
    padding: PAD,
  },
  thumb: { 
    borderRadius: RADIUS/2,
    padding: 0
  },
});