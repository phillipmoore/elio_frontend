/**
 * Learn more about deep linking with React Navigation
 * https://reactnavigation.org/docs/deep-linking
 * https://reactnavigation.org/docs/configuring-links
 */

import { LinkingOptions } from '@react-navigation/native';
import * as Linking from 'expo-linking';

import { RootStackParamList } from '../types';

const linking: LinkingOptions<RootStackParamList> = {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Root: {
        screens: {
          UserLocationList: {
            screens: {
              UserLocationListTab: 'one',
            },
          },
          Messages: {
            screens: {
              MessagesListTab: 'two',
            },
          },
          Connections: {
            screens: {
              ConnectionListTab: 'three',
            },
          },
          MyProfileDetail: {
            screens: {
              MyProfileDetailTab: 'four',
            },
          },

        },
      },
      ModalNavigator: {
        screens: {
          Modal: 'modal',
          ProfileEditInput: '*',
        }
      },
      NotFound: '*',
      ProfileDetail: '*',
      ConnectionListScreen: '*'
    },
  },
};

export default linking;
