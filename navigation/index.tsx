/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import { ColorSchemeName, Pressable, Text } from 'react-native';
import { connect } from 'react-redux'

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import ModalScreen from '../screens/ModalScreen';
import ProfileEditInput from '../screens/ProfileEditInput';
import UserLocationListTab from '../screens/UserLocationListTab';
import ConnectionListTab from '../screens/ConnectionListTab';
import MyProfileDetailTab from '../screens/MyProfileDetailTab';
import MessageListTab from '../screens/MessageListTab';
import ProfileDetailScreen from '../screens/ProfileDetailScreen';
import AuthenticationScreen from '../screens/AuthenticationScreen';
import ConnectionListScreen from '../screens/ConnectionListScreen';
import MessageListScreen from '../screens/MessageListScreen';
import ContextModal from '../screens/ContextModal';
import WebViewModal from '../screens/WebViewModal';
import { RootStackParamList, RootTabParamList, RootTabScreenProps } from '../types';
import LinkingConfiguration from './LinkingConfiguration';
import TabBarButton from '../components/TabBarButton'


const Navigation = (props: {accessToken: string, expiresIn: number, setUser: any, setInitialTokens: any}, { colorScheme }: { colorScheme: ColorSchemeName }) => { 
  return !!props.accessToken && Date.now() < props.expiresIn? (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}
    >
      <RootNavigator />
    </NavigationContainer>
  ) : ( 
    <AuthenticationScreen />
  )  
}

const mapStateToProps = (state: {userReducer: {initialTokens: {access_token: string, expires_in: number}}}) => {
  return {
    accessToken: state.userReducer.initialTokens.access_token,
    expiresIn: state.userReducer.initialTokens.expires_in,
  }
}

export default connect(mapStateToProps)(Navigation)

/**
 * A root stack navigator is often used for displaying modals on top of all other content.
 * https://reactnavigation.org/docs/modal
 */
const Stack = createNativeStackNavigator<RootStackParamList>();

function ModalNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{ headerBackTitle: "" }}
    >
      <Stack.Screen 
        name="EditProfile" 
        component={ModalScreen} 
        options={({ navigation }: any) => ({
          headerLeft: () => (
            <Pressable
              onPress={() => navigation.navigate('Root')}
              style={({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
              })}>
              <Text>Cancel</Text>
            </Pressable>
          ),
          headerRight: () => (
            <Pressable
              onPress={() => navigation.navigate('EditProfile', {save: true})}
              style={({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
              })}>
              <Text>Done</Text>
            </Pressable>
          )
        })}
      />
      <Stack.Screen 
        name="ProfileEditInput" 
        component={ProfileEditInput}
        options={({ navigation }: any) => ({
          headerRight: () => (
            <Pressable
              onPress={() => navigation.navigate('ProfileEditInput', {save: true})}
              style={({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
              })}>
              <Text>Done</Text>
            </Pressable>
          )
        })}
      />
    </Stack.Navigator>
  );
}

function RootNavigator({ notifications }: any) {
  return (
    <Stack.Navigator
      screenOptions={{ headerBackTitle: "" }}
    >
      <Stack.Screen name="Root" component={BottomTabNavigator} options={{ headerShown: false }} />
      <Stack.Screen name="ProfileDetail" component={ProfileDetailScreen} options={{ title: 'Details!' }} />
      <Stack.Screen name="ConnectionListScreen" component={ConnectionListScreen} />
      <Stack.Screen name="MessageListScreen" component={MessageListScreen} />
      <Stack.Screen
        name="ModalNavigator"
        component={ModalNavigator}
        options={{ 
          presentation: 'transparentModal', 
          headerShown: false 
        }}
      />
      <Stack.Screen 
        name="ContextModal"
        component={ContextModal}
        options={{ 
          presentation: 'modal', 
          headerShown: false,
          contentStyle: {
            backgroundColor:'rgba(255,255,255,0)'
          } 
        }}
      />
      <Stack.Screen 
        name="WebView" 
        component={WebViewModal} 
        options={({ navigation }: any) => ({
          presentation: 'modal',
          headerLeft: () => (
            <Pressable
              onPress={() => navigation.navigate('Root')}
              style={({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
              })}>
              <Text>Cancel</Text>
            </Pressable>
          ),
          headerRight: () => (
            <Pressable
              onPress={() => navigation.navigate('EditProfile', {save: true})}
              style={({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
              })}>
              <Text>Done</Text>
            </Pressable>
          )
        })}
      />
    </Stack.Navigator>
  );
}

/**
 * A bottom tab navigator displays tab buttons on the bottom of the display to switch screens.
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */
const BottomTab = createBottomTabNavigator<RootTabParamList>();

function BottomTabNavigator() {
  const colorScheme = useColorScheme();
  const tabBarBadgeStyle = {
    backgroundColor: 'crimson', 
    transform: [{scaleX: 0.32}, {scaleY: 0.32}],
    top: 38,
    left: -9.5,
  }
  const tabBarBeaconStyle = {
    backgroundColor: 'transparent', 
    // transform: [{scaleX: 0.32}, {scaleY: 0.32}],
    top: 38,
    left: -8,
    // borderRadius: 12,
    // width: 24,
    // height: 24,

  }

  // const locationNotifications = notifications.locations.id_list.length > 0 ? '' : undefined
  // const messageNotifications = notifications.messages.id_list.length > 0 ? '' : undefined
  // const connectionsNotifications = notifications.connections.id_list.length > 0 ? '' : undefined


  return (
    <BottomTab.Navigator
      initialRouteName="UserLocationList"
      screenOptions={{
        tabBarActiveTintColor: Colors[colorScheme].tint,
      }}
    >      
      <BottomTab.Screen
        name="UserLocationList"
        component={UserLocationListTab}
        // ({ navigation }: RootTabScreenProps<'TabOne'>) => (
        options={{
          title: 'Home',
          tabBarLabelStyle: {
            display: 'none'
          },
          tabBarButton: props => <TabBarButton sectionName="locations" {...props} />
        }}
      />
      <BottomTab.Screen
        name="Messages"
        component={MessageListTab}
        options={{
          title: 'Messages',
          tabBarLabelStyle: {
            display: 'none'
          },
          tabBarButton: props => <TabBarButton sectionName="messages" {...props} />,
        }}
      />
      <BottomTab.Screen
        name="Connections"
        component={ConnectionListTab}
        options={{
          title: 'Connections',
          tabBarLabelStyle: {
            display: 'none'
          },
          tabBarButton: props => <TabBarButton sectionName="connections" {...props} />
        }}
      />
      <BottomTab.Screen
        name="MyProfileDetail"
        component={MyProfileDetailTab}
        options={({ navigation }: RootTabScreenProps<'MyProfileDetail'>) => ({
          title: 'Profile',
          tabBarLabelStyle: {
            display: 'none'
          },
          tabBarButton: props => <TabBarButton sectionName="profile" {...props} />,
          headerRight: () => (
            <Pressable
              onPress={() => navigation.navigate('ModalNavigator')}
              style={({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
              })}>
              <Ionicons
                name="create-outline"
                size={25}
                color={Colors[colorScheme].text}
                style={{ marginRight: 15 }}
              />
            </Pressable>
          ),
        })}
      />
    </BottomTab.Navigator>
  );
}

/**
 * You can explore the built-in icon families and icons on the web at https://icons.expo.fyi/
 */
// function TabBarIcon(props: {
//   name: React.ComponentProps<typeof Ionicons>['name']
//   color: string
// }) {
//   return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />
// }
