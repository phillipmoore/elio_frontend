import {
  SET_HEADER_HEIGHT, 
  SET_PROFILE_CONTENT_HEIGHT,
  SET_BOTTOM_SCREEN_COORDS,
  SET_TOP_SCREEN_COORDS,
  SET_BOTTOM_CONTAINER_OPEN,
  SET_TOP_CONTAINER_OPEN,
  SET_SCALE_TOP,
  SET_SCALE_BOTTOM,
  SET_PAN_RESPONDER_PARAMS
} from '../actions/types'

const initialState = {
  headerHeight: 0,
  profileContentHeight: 0,
  bottomScreenCoords: {},
  topScreenCoords: {},
  bottomContainerOpen: false,
  topContainerOpen: false,
  scaleTop: { value: 0 },
  scaleBottom: { value: 0 },
  panResponderParams: { ref: null, lastIndex: 0, touched: false }
}

const animationReducer = (state = initialState, action: {type: string; data: number}) => {
  switch(action.type) {
    case SET_HEADER_HEIGHT:
      return {
        ...state,
        headerHeight: action.data
      };
    case SET_PROFILE_CONTENT_HEIGHT:
      return {
        ...state,
        profileContentHeight: action.data 
      };
    case SET_BOTTOM_SCREEN_COORDS:
      return {
        ...state,
        bottomScreenCoords: action.data 
      };
    case SET_TOP_SCREEN_COORDS:
      return {
        ...state,
        topScreenCoords: action.data 
      };
    case SET_BOTTOM_CONTAINER_OPEN:
      return {
        ...state,
        bottomContainerOpen: action.data 
      };
    case SET_TOP_CONTAINER_OPEN:
      return {
        ...state,
        topContainerOpen: action.data 
      };
    case SET_SCALE_TOP:
      return {
        ...state,
        scaleTop: action.data 
      };
    case SET_SCALE_BOTTOM:
      return {
        ...state,
        scaleBottom: action.data 
      };
    case SET_PAN_RESPONDER_PARAMS:
      return {
        ...state,
        panResponderParams: action.data
      }

    default:
      return state;
  }
}

export default animationReducer;