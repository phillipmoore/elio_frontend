import {SET_IG_RESOURCE} from '../actions/types'

const initialState = {
  igResource: 0
}

const resourceReducer = (state = initialState, action: {type: string; data: number}) => {
  switch(action.type) {
    case SET_IG_RESOURCE:
      return {
        ...state,
        igResource: action.data 
      };

    default:
      return state;
  }
}

export default resourceReducer;