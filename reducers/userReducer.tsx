import { 
  SET_INITIAL_TOKENS, 
  SET_USER,
  SET_PROFILE,
  SET_UPDATE,
  SET_NOTIFICATIONS,
  SET_USER_LOCATION,
  SET_INSTAGRAM_OBJECT,
  SET_TOP_DATA,  
} from '../actions/types'

import { UserStateType, UserActionType } from '../types/userTypes'

const emptyInitialTokens = {
  access_token: "",
  expires_in: 0
}

const emptyUser = {
  id: 0,
  email: "",
  name: "",
  last_profile_id: 0
}

const emptyProfile = {
  id: 0,
  username: "",
  bio: "",
  avatar: "",
  connection_count: 0,
  mutual_count: 0,
  is_visable: true,
  notifications: true,
  blocked_profiles: [],
  resource_order: null,
  connection_to_user: null
}

const emptyUpdate = new Date()

const emptyNotifications = {
  locations: { id_list: [] }, 
  messages: { id_list: [] }, 
  connections: { id_list: [] },
}

const emptyUserLocation = {
  latitude: 0,
  longitude: 0, 
  location_time: new Date()
}

const initialState = {
  initialTokens: emptyInitialTokens,
  user: emptyUser,
  profile: emptyProfile,
  udpate: emptyUpdate,
  notifications: emptyNotifications,
  userLocation: emptyUserLocation,
  instagramObject: {},
  topData: [],
}

const userReducer = (state: UserStateType = initialState, action: UserActionType) => {
  switch(action.type) {
    case SET_INITIAL_TOKENS:
      return {
        ...state,
        initialTokens: action.data 
      };
    case SET_USER:
      return {
        ...state,
        user: action.data
      };
    case SET_PROFILE:
      return {
        ...state,
        profile: action.data
      };
    case SET_UPDATE:
      return {
        ...state,
        update: action.data
      };
    case SET_NOTIFICATIONS:
      return {
        ...state,
        notifications: action.data
      };
    case SET_USER_LOCATION:
      return {
        ...state,
        userLocation: action.data
      };
    case SET_INSTAGRAM_OBJECT:
      return {
        ...state,
        instagramObject: action.data
      };
    case SET_TOP_DATA:
      return {
        ...state,
        topData: action.data 
      };
    default:
      return state;
  }
}

export default userReducer;