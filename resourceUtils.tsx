import axios from 'axios'
import InstagramBasicDisplayApi from 'instagram-basic-display'

import { apiUrl,apiHeader } from  './utils'

import store from './store'

// https://developers.facebook.com/apps/642185700405463/instagram-basic-display/basic-display/
// TODO: change redirectUrl to a blank page on backend server when server via https
export const redirectUrl = 'https://example.com/'

const getProfileAndToken = () => {
  const state = store.getState()
  const obj: { profileId: number, accessToken: string } = { profileId: 0, accessToken: '' }
  obj.profileId = state.userReducer.profile.id
  obj.accessToken = state.userReducer.initialTokens.access_token
  return obj
}

export const igObj = new InstagramBasicDisplayApi({
  appId: '557474545247859',
  redirectUri: redirectUrl,
  appSecret: 'c15a1ead33088e2825cad726ae6d7bd6'
})

export const loginToIg = async (igCode: any) => {
  const { profileId, accessToken } = getProfileAndToken()

  const shortTokenData = await igObj.retrieveToken(igCode)
  const longTokenData = await igObj.retrieveLongLivedToken(shortTokenData.access_token)
  const expiresIn = new Date(new Date().getTime() + longTokenData.expires_in)
  const profileData = JSON.stringify({ token: longTokenData.access_token, token_expiration: expiresIn })

  const response = await axios.patch(
      `${apiUrl}/profile-resources/${profileId}/ig/`, 
      profileData, 
      apiHeader(accessToken)
    ).catch((err) => {
      console.log('patch profile-resources error: ', err)
    })
  return response
}

const refreshIgToken = async (LongLivedToken: string) => {
  const { profileId, accessToken } = getProfileAndToken()

  const longTokenData = await igObj.refreshLongLivedToken(LongLivedToken)
  const expiresIn = new Date(new Date().getTime() + longTokenData.expires_in)
  const profileData = JSON.stringify({ token: longTokenData.access_token, token_expiration: expiresIn })
  const response = await axios.patch(`${apiUrl}/profile-resources/${profileId}/ig/`, profileData, apiHeader(accessToken))
  return response.data.token
}

export const getIgToken = async () => {
  const { profileId, accessToken } = getProfileAndToken()
  const res = await axios.get(`${apiUrl}/profile-resources/${profileId}/ig/`, apiHeader(accessToken))

  if (new Date(res.data.token_expiration) > new Date()) {
    return refreshIgToken(res.data.token)
  }

  return res.data.token
}

export const getIgMedia = async (itemNumber: number) => {
  const token = await getIgToken()
  const res = await igObj.retrieveUserMedia(token, itemNumber).catch((err: any) => console.log('retrieveUserMedia error: ', err)) 
  return res ? res : []
}

export const getIgMediaData = async (id: number, fields: string) => {
  const token = await getIgToken()
  const res = await igObj.retrieveMediaData(token, id, fields).catch((err: any) => console.log('retrieveUserMedia error: ', err)) 
  return res
}


