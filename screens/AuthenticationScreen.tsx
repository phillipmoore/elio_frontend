import axios, { AxiosResponse } from 'axios'
import _ from 'lodash'
import * as React from 'react'
import { StyleSheet, TouchableOpacity} from 'react-native'
import { Button, Input } from 'react-native-elements'
import { useToast } from "react-native-toast-notifications"

import { connect, ConnectedProps } from 'react-redux'
import { setInitialTokens, setUser, setProfile } from '../actions/user'
import store from '../store'

import { Text, View } from '../components/Themed'
import { apiUrl, apiHeader, isEmail, getData } from '../utils'
import { UserActionType, UserStateType } from '../types/userTypes'
import { Action } from 'redux'

const getExpirationDate = () => {
  let dateObj = Date.now()
  dateObj += 1000 * 60 * 60 * 10
  return dateObj
}

export const setUserAndInitialTokens = (
    grantType: string, 
    email: string, 
    password: string, 
    refreshToken: string
  ) => {
  /* A function to login or to refresh access token 
    TODO: grab the client id & client secret from elsewhere
  */

  const data: any = {
    client_id: "fIipzD0d9FMqlPK4IasFlUA5ga6vGkWX9nCoTFJL",
    client_secret: "S44ksXbw4bCHTDLkcpRkuHXoMCuSgo70wWBpTNmxRsy5IIlRnNJlwZZOggLeKVBhp5h4SCZYPdvzrzYdfgOnXRqiCWok8tXsUK5l4pEaEOJpoJhMYq4CKUbyfqVCecxI",
  }

  if (grantType === 'password') {
    data['grant_type'] = grantType
    data['username'] = email
    data['password'] = password
  } else if (grantType === 'refresh_token') {
    data['grant_type'] = grantType
    data['refresh_token'] = refreshToken
  }

  return axios.post(`${apiUrl}/auth/token`, data).then((iTokenRes: AxiosResponse<UserStateType['initialTokens']>)  => {
    // if we get an access token, then we attempt to use to get user
    
    if (!!iTokenRes.data.access_token) {
      const formattedEmail = email.replaceAll('.', '<dot>') // sadly, we have to format email to use email as lookup field in DRF
      console.log(formattedEmail)
      return axios.get(`${apiUrl}/users/${formattedEmail}/`, apiHeader(iTokenRes.data.access_token)).then((userRes: AxiosResponse<UserStateType['user']>) => {   
        console.log(userRes.data.last_profile_id)
        return axios.get(`${apiUrl}/profiles/${userRes.data.last_profile_id}/`, apiHeader(iTokenRes.data.access_token)).then((profileRes: AxiosResponse<UserStateType['profile']>) => {
          // set the initialTokens
          if (!!iTokenRes.data.expires_in) {
            // change expiration to actual date
            iTokenRes.data.expires_in = getExpirationDate()
          }
          store.dispatch(setInitialTokens(iTokenRes.data))
          // set user
          store.dispatch(setUser(userRes.data))
          // set Profile
          store.dispatch(setProfile(profileRes.data))
          // return user response
          return userRes
        })        
      })
    }
  })
}

// ---------- AuthenticationScreen ----------- //

const mapStateToProps = (state: { userReducer: UserStateType } ) => {
  return {
    initialTokens: state.userReducer.initialTokens,
    profile: state.userReducer.profile,
  }
}

const mapDispatchToProps = {
    setInitialTokens,
    setUser,
    setProfile
}

const connector = connect(mapStateToProps, mapDispatchToProps)
type PropsFromRedux = ConnectedProps<typeof connector>
export type Props = PropsFromRedux

const AuthenticationScreen = (props: Props) => {
  const [isCheckingLogin, setIsCheckingLogin] = React.useState(true)
  // form fields and validation
  const [email, setEmail] = React.useState("") 
  const [name, setName] = React.useState("") 
  const [username, setUsername] = React.useState("") 
  const [password, setPassword] = React.useState("") 
  const [verifiedPassword, setVerifiedPassword] = React.useState("")
  const [screenIsLogin, setScreenIsLogin] = React.useState(true) 
  const [signInSuccessMessage, setSignInSuccessMessage] = React.useState("") 
  const [formIsShowing, setFormIsShowing] = React.useState(true)

  const toast = useToast()

  React.useEffect(() => {
    checkOrSetLogin()
  }, [props.initialTokens.access_token])

  const checkOrSetLogin: any = async () => {
    const initialTokens = await getData('@initialTokens')
    const user = await getData('@user')
    const profile = await getData('@profile')
    const hasAccessToken = !!initialTokens && !!initialTokens.access_token
    const isNotExpired = !!initialTokens && Date.now() < initialTokens.expires_in

    if ( hasAccessToken && isNotExpired ) {
      // setting initialTokens and user from async storage
      setIsCheckingLogin(false)

      props.setUser(user)
      props.setProfile(profile)
      props.setInitialTokens(initialTokens)
    } else if ( hasAccessToken && !isNotExpired ) {
      // refreshing the expired access token and setting 
      setUserAndInitialTokens('refresh_token', user.email, '', initialTokens.refresh_token).then(() => {
        setIsCheckingLogin(false)
      }).catch(err => {
        // issues with refreshing access token, open up login screen
        console.log(err)
        setIsCheckingLogin(false)
      })
    } else {
      // no access token in async storage. go straight to login screen
      setIsCheckingLogin(false)
    }
  }

  const login = () => {
    if (isEmail(email) && !!password) {
      setUserAndInitialTokens('password', email, password, '').then(userRes => {
        const username = _.get(userRes, ['data', 'username'], "")
        toast.show(`Welcome ${username}!`, {type: 'success'})
      }).catch(error => {
        console.log(error)
        toast.show("Email or password is incorrect", {type: 'danger'})
      })
    } else {
      if (!email  || !password) {
        toast.show("You must fill in all fields", {type: 'danger'})
      }
      if (!isEmail(email) && !!email) {
        toast.show(`${email} is not a valid email address`, {type: 'danger'})
      }
    }
  }

  const signUp = () => {
    if (isEmail(email) && !!name && !!username && (password === verifiedPassword)) { 
      const data = {
        email: email,
        first_name: name,
        last_name: username,
        password: password
      }
      const _postProspectiveUser = () => {
        axios.post(`${apiUrl}/prospective_users/`, data).then(() => {
            // on success of creating prospective user, clear fields and swith to Login
            setEmail("")
            setName("")
            setUsername("") 
            setPassword("") 
            setVerifiedPassword("")
            setScreenIsLogin(true)
            toast.show('Check your email inbox to confirm account.', {type: 'success'})
          }).catch(error => {
            console.log(error)
            toast.show('Hmmm, there was a problem.  Please try re-submitting.', {type: 'danger'})
        })
      }

      // attempt to login with email and password provided before signing up. just incase user with this email and password exist
      setUserAndInitialTokens('password', email, password, '').then(userRes => {
        const uName = _.get(userRes, ['data', 'username'], "")
        toast.show(`Welcome ${uName}!`, {type: 'success'})
      }).catch(error => {
        console.log(error)
        // check to see if prospective user already exists. if so, delete and make new one
        const formattedEmail = email.replaceAll('.', '<dot>')
        axios.get(`${apiUrl}/prospective_users/${formattedEmail}/`).then(() => {
          // prospective user already exists, delete and create a new one
          axios.delete(`${apiUrl}/prospective_users/${formattedEmail}/`).then(() => {
            _postProspectiveUser()
          })
        }).catch (error=> {
          // prospective user does not exist yet. create one now
          console.log(error)
          _postProspectiveUser()
        })
      })

    } else {
      if (password !== verifiedPassword) {
        toast.show("Please make sure the passwords match", {type: 'danger'})
      }
      if (!email || !name || !username || !password || !verifiedPassword) {
        toast.show("You must fill in all fields", {type: 'danger'})
      }
      if (!isEmail(email) && !!email) {
        toast.show(`${email} is not a valid email address`, {type: 'danger'})
      }
    }
  }

  const toggleLoginSignup = () => {
    setScreenIsLogin(!screenIsLogin)
  }

  const sendPasswordResetEmail = () => {
    if (isEmail(email)) {
      axios.get(`${apiUrl}/forgot_password/${email}`).then(res => {
        toast.show('Check your email inbox for instructions on how to reset password.', {type: 'success'})
      })
    } else {
      if (!isEmail(email) && !!email) {
        toast.show(`${email} is not a valid email address`, {type: 'danger'})
      }
      if (!email) {
        toast.show(`please enter your email address`, {type: 'danger'})
      }
    }
  }

  return ( isCheckingLogin ?
    <View style={styles.container}><Text>Is Checking Login</Text></View> :
    <View style={styles.container}>
      <Text style={styles.title}>Microconomy</Text>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      { formIsShowing ?
        (<View style={{width: 350}}>
          <Input
            placeholder='email@address.com'
            leftIcon={{ type: 'material-icons', name: 'email' }}
            onChangeText={(value: string) => setEmail(value)}
            autoCapitalize="none"
            value={email}
          />
          {
            screenIsLogin || (
              <View >
                <Input
                  placeholder='Name'
                  leftIcon={{ type: 'material-icons', name: 'person' }}
                  onChangeText={(value: string) => setName(value)}
                  autoCapitalize="none"
                  value={name}
                />
                <Input
                  placeholder='Username'
                  leftIcon={{ type: 'material-icons', name: 'person' }}
                  onChangeText={(value: string) => setUsername(value)}
                  autoCapitalize="none"
                  value={username}
                />
              </View>
            )
          }
          <Input
            placeholder='Password'
            leftIcon={{ type: 'material-icons', name: 'lock' }}
            onChangeText={(value: string) => setPassword(value)}
            autoCapitalize="none"
            value={password}
            // secureTextEntry={true}
          />
          {
            screenIsLogin || (
              <Input
                placeholder='Verify Password'
                leftIcon={{ type: 'material-icons', name: 'lock' }}
                onChangeText={(value: string) => setVerifiedPassword(value)}
                autoCapitalize="none"
                value={verifiedPassword}
                // secureTextEntry={true}
              />
            )
          }
          {
            screenIsLogin ? (
              <Button 
                title="Log In"
                onPress={() => login()}
              />
            ) : (
              <Button 
                title="Sign Up"
                onPress={() => signUp()}
              />
            )
          }
        </View>) : (
          <Text>Some sort of success message</Text>
        )
      }
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <TouchableOpacity onPress={() => toggleLoginSignup()}>
        <Text>
          { screenIsLogin ? (<Text>Don't </Text>) : (<Text>Already </Text>)}
          have an account?
        </Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => sendPasswordResetEmail()}>
        <Text>Forgot Password</Text>
      </TouchableOpacity>
    </View>
  )
}

export default connector(AuthenticationScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
})
