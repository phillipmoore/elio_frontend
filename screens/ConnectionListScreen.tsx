import axios from 'axios'
import React, { useState, useEffect } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { NativeStackScreenProps } from '@react-navigation/native-stack'

import { apiUrl, apiHeader } from '../utils'
import ConnectionList from '../components/ConnectionList'

import { UserStateType } from '../types/userTypes'
import { RootStackParamList } from '../types'

const mapStateToProps = (state: { userReducer: UserStateType }) => {
  return {
    initialTokens: state.userReducer.initialTokens,
    profile: state.userReducer.profile
  }
}

const connector = connect(mapStateToProps)
type PropsFromRedux = ConnectedProps<typeof connector>
type OwnProps = NativeStackScreenProps<RootStackParamList, 'ConnectionListScreen'>
type Props = PropsFromRedux & OwnProps

function ConnectionsListScreen(props: Props) {
  // this screen is access
  const profile = props.route.params.profile // profile we are checking connections against
  const tabIndex = props.route.params.tabIndex
  const loggedInProfile = props.profile // person who is logged into app, obvs
  const [connections, setConnections] = useState([])
  const [mutualConnections, setMutualConnections] = useState([])

  useEffect(() => {
    axios.get(`${apiUrl}/connections/?profile_id=${profile.id}`, apiHeader(props.initialTokens.access_token)).then(res => {
      setConnections(res.data.results)
    })
    if (loggedInProfile.id !== profile.id) {
      axios.get(`${apiUrl}/connections/?profile_id=${profile.id}&mutual=True`, apiHeader(props.initialTokens.access_token)).then(res => {
        setMutualConnections(res.data.results)
      })
    }
  },[])

  return (
    <ConnectionList 
      connections={connections} 
      mutualConnections={mutualConnections} 
      profile={profile} 
      tabIndex={tabIndex}
      fromTabView={false}
    /> 
  )
}

export default connect(mapStateToProps)(ConnectionsListScreen)
