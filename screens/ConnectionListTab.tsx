import axios from 'axios'
import React, { useState, useEffect } from 'react'
import { Alert } from 'react-native'
import { connect, ConnectedProps } from 'react-redux'
import { useIsFocused } from "@react-navigation/native"
import { NativeStackScreenProps } from '@react-navigation/native-stack'

import { apiUrl, apiHeader } from '../utils'
import ConnectionList from '../components/ConnectionList'
import { resetNotifications } from '../components/TaskComponent'

import { UserStateType } from '../types/userTypes'
import { ConnectionInterface, ConnectionDataInterface, NestedActionType } from '../types/modelTypes'
import { RootStackParamList } from '../types'

const mapStateToProps = (state: { userReducer: UserStateType }) => {
  return {
    initialTokens: state.userReducer.initialTokens,
    profile: state.userReducer.profile,
    update: state.userReducer.update,
  }
}

const connector = connect(mapStateToProps)
type PropsFromRedux = ConnectedProps<typeof connector>
type OwnProps = NativeStackScreenProps<RootStackParamList, 'ConnectionListScreen'>
type Props = PropsFromRedux & OwnProps

function ConnectionsListTab({ initialTokens, profile, update, navigation }: Props) {
  const [connections, setConnections] = useState([])
  const isFocused = useIsFocused()

  useEffect(() => {
    if(isFocused) {
      getConnections()
    } 
    
  },[update, isFocused])

  const getConnections = () => {
    axios.get(`${apiUrl}/connections/?profile_id=${profile.id}`, apiHeader(initialTokens.access_token)).then(res => {
      setConnections(res.data.results)
      resetNotifications(profile.id, initialTokens.access_token)
    }).catch(err => { console.log(JSON.stringify(err)) })
  }

  const promptUserToggle = () => {
    Alert.alert(
      "Pending",
      "Waiting for user to <3 you back",
      [
        { text: "OK", onPress: () => console.log("OK Pressed") }
      ]
    )
  }

  const toggleConnectionAccept = (connection: any) => {
    const isInitiator = profile.id === connection.initiator.id
    const whichHidden = isInitiator ? 'initiator_hidden' : 'responder_hidden'

    // TODO: add prompt to delete connection forever

    if (isInitiator && !connection.accepted) {
      promptUserToggle()
      return
    }

    const data: ConnectionDataInterface = {
      accepted: !connection.accepted
    }
    data[whichHidden] = false
    const JSONdata = JSON.stringify(data)


    axios.patch(`${apiUrl}/connections/${connection.id}/`, JSONdata, apiHeader(initialTokens.access_token)).then(patchRes => {
      getConnections()
    }).catch(err => { console.log(JSON.stringify(err)) })
  }

  const toggleConnectionVisibility = (connection: ConnectionInterface['item']) => {
    const data: ConnectionDataInterface = {}
    const isInitiator = profile.id === connection.initiator.id
    const whichBool = isInitiator ? connection.initiator_hidden : connection.responder_hidden
    const whichHidden = isInitiator ? 'initiator_hidden' : 'responder_hidden'

    data[whichHidden] = !whichBool
    data.accepted = isInitiator ? connection.accepted : false

    const JSONdata = JSON.stringify(data)

    axios.patch(`${apiUrl}/connections/${connection.id}/`, JSONdata, apiHeader(initialTokens.access_token)).then(patchRes => {
      getConnections()
    }).catch(err => { console.log(JSON.stringify(err)) })

  }

  const nestedAction = (params: NestedActionType) => {
    switch (params.type) {
      case 'complete-connection':
        toggleConnectionAccept(params.connection)
        break
      case 'visit-profile':
        navigation.navigate('ProfileDetail', { profile: params.otherProfile })
        break

      case 'show-hide-connection':
        toggleConnectionVisibility(params.connection)
        break
    }
  }

  return (
    <ConnectionList 
      connections={connections} 
      profile={profile} 
      mutualConnections={[]} 
      fromTabView={true}
      nestedAction={nestedAction}
    /> 
  )
}

export default connector(ConnectionsListTab)
