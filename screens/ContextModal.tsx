import axios from 'axios'
import _ from 'lodash'
import React from 'react'
import { 
  StyleSheet,  
  TouchableOpacity, 
  NativeSyntheticEvent, 
  NativeTouchEvent,
} from 'react-native'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { connect, ConnectedProps } from 'react-redux'
import { setProfile } from '../actions/user'
import { apiUrl, apiHeader } from '../utils'
import { Text, View, Button } from '../components/Themed'
import { toggleVisibility } from './UserLocationListTab'

import { UserStateType } from '../types/userTypes'
import { RootStackParamList } from '../types'

const mapStateToProps = (state: { userReducer: UserStateType }) => {
  return {
    initialTokens: state.userReducer.initialTokens,
    user: state.userReducer.user,
    profile: state.userReducer.profile
  }
}

const mapDispatchToProps =  {
  setProfile
}

const connector = connect(mapStateToProps, mapDispatchToProps)
type PropsFromRedux = ConnectedProps<typeof connector>
type OwnProps = NativeStackScreenProps<RootStackParamList, 'ConnectionListScreen'>
type Props = PropsFromRedux & OwnProps


function ContextModal({ navigation, route, profile, initialTokens, setProfile }: Props) {
  // const [numberOfConnections, setNumberOfConnections] = useState(0)
  const owner = route.params.profile
  const isVisible = profile.is_visible

  const closeModal = () => {
    navigation.pop()
  }

  const addOrRemoveProfileToBlocked = (e: NativeSyntheticEvent<NativeTouchEvent>) => {
    const blocked = _.cloneDeep(profile.blocked_profiles)
    const ownerIndex = blocked.indexOf(owner.id)
    if( ownerIndex === -1){
      blocked.push(owner.id)
    } else {
      blocked.splice(ownerIndex, 1)
    }
    const data = JSON.stringify({
      blocked_profiles: blocked
    })
    axios.patch(`${apiUrl}/profiles/${profile.id}/`, data, apiHeader(initialTokens.access_token)).then(res => {
      setProfile(res.data)
      closeModal()
    })
  }
  const doesInclude = profile.blocked_profiles.includes(owner.id)
  const blockText = doesInclude ? "Be visible to " : "Hide from "
  const visibleText = !profile.is_visible ? "Be visible to everyone": "Hide from everyone"

  return (
    <TouchableOpacity 
      style={styles.backdrop}
      onPress={() => {
        closeModal()
      }}
      activeOpacity={1}
    >
      <TouchableOpacity style={styles.container} activeOpacity={1}>
        <View style={styles.handle}></View>
        <View style={styles.containerView}>
        {isVisible &&
          <Button onPress={(e: NativeSyntheticEvent<NativeTouchEvent>) => {
            addOrRemoveProfileToBlocked(e)
          }}>
            <Text>{blockText}{owner.username}</Text>
          </Button>
        }
          <View style={{ margin: 4 }}/>
          <Button onPress={() => {
            // TODO: ponder whether to clear all hidden profiles here
            toggleVisibility({ profile, initialTokens, setProfile }).then(res => {
              closeModal()
            })
          }}>
            <Text>{visibleText}</Text>
          </Button>
        </View>
      </TouchableOpacity>
    </TouchableOpacity>
  )
}

export default connector(ContextModal)

const styles = StyleSheet.create({
  backdrop: {
    height: '100%',
    justifyContent: 'flex-end',
    backgroundColor:'rgba(255,255,255,0)',
    position: 'relative',
  },
  container: {
    alignItems: 'center',
    backgroundColor: 'rgba(255,255,255,1)',
    height: '60%',
    width: '100%',
    borderRadius: 12,
    position: 'absolute',
    top:"50%",
    left:0,
  },
  handle: {
    width: 40,
    height: 4,
    margin: 8,
    backgroundColor: 'lightgrey',
    borderRadius: 2
  },
  containerView: {
    width: '100%',
    backgroundColor:'rgba(255,255,255,0)',
    padding: 20,
    // borderWidth: 1
  },
  containerText: {
    color: '#fff'
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
})
