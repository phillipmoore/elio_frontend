import axios from 'axios'
import React, { useState, useEffect, useRef } from 'react'
import { StyleSheet, FlatList, TouchableOpacity } from 'react-native'
import { connect, ConnectedProps } from 'react-redux'
import { useIsFocused } from "@react-navigation/native"
import { NativeStackScreenProps } from '@react-navigation/native-stack'

import { apiUrl, apiHeader, formatDateTime } from '../utils'
import { Text, View, TextInput } from '../components/Themed'
import Wave from '../components/Wave'

import { UserStateType } from '../types/userTypes'
import { MessageInterface } from '../types/modelTypes'
import { RootStackParamList } from '../types'

interface MessageListItemInterface {
  message: MessageInterface
  lastMessageDate: string
  profile: UserStateType['profile']
} 

const MessageListItem = ({ message, lastMessageDate, profile }: MessageListItemInterface) => {
  const direction = profile.id !== message.item.sender_id ? 'Left' : 'Right'

  const DateDisplay = ({ thisDateString, lastDateString }: any) => {
    const lastDate: any = new Date(lastDateString)
    const thisDate: any = new Date(thisDateString)
    const timeDiff = thisDate - lastDate
    const isVisible = timeDiff > 300000 // 5 minutes

    return ( isVisible && lastMessageDate ?
      <View style={styles.dateTime}>
        <Text style={styles.dateTimeText}>{formatDateTime(lastMessageDate)}</Text>
      </View> : null
    )
  }

  const TextBubble = ({ text }: any) => {
    return <View style={styles[`message${direction}`]}><Text>{text}</Text></View>
  }

  const WaveBubble = () => {
    return (
      <View style={[styles[`message${direction}`], {backgroundColor: undefined}]}>
        <Wave direction={direction.toLowerCase()} />
      </View>
    )
  }

  return (
    <View 
      key={`message_${message.index}`}
      style={styles[`messageContainer${direction}`]}
    >
      <DateDisplay thisDateString={message.item.date_sent} lastDateString={lastMessageDate} />
      {message.item.text.toLowerCase() !== 'hi' ?
        <TextBubble text={message.item.text} /> :
        <WaveBubble /> 
      }

    </View>
  )
}

const mapStateToProps = (state: any) => {
  return {
    initialTokens: state.userReducer.initialTokens,
    profile: state.userReducer.profile,
    update: state.userReducer.update,
  }
}

const connector = connect(mapStateToProps)
type PropsFromRedux = ConnectedProps<typeof connector>
type OwnProps = NativeStackScreenProps<RootStackParamList, 'MessageListScreen'>
type Props = PropsFromRedux & OwnProps

function MessageListScreen({profile, initialTokens, route, update}: Props) {
  // const tabIndex = route.params.tabIndex
  const isFocused = useIsFocused()
  const [messages, setMessages] = useState<MessageInterface['item'][]>([])
  const [messageText, setMessageText] = useState('')
  let listRef = useRef<FlatList>(null)

  useEffect(() => {
    getAllMessages()
    // the update interval is 10 sec. In order to 
    // increase the updates on when this component 
    // receives messages we a add a setTimeout for 5 sec
    // TODO: add more rapid update if more uses arise
    setTimeout(() => {
      getAllMessages()
    }, 5000)
  },[update, isFocused])

  const markMessageAsRead = (msgId: any) => {
    const data =JSON.stringify({
      was_opened: true
    })
    axios.patch(`${apiUrl}/messages/${msgId}/`, data, apiHeader(initialTokens.access_token))
    .catch(err => console.log(err))
  }

  const getAllMessages = () => {
    return axios.get(`${apiUrl}/messages/?connection=${route.params.connectionId}`, apiHeader(initialTokens.access_token)).then(res => {
      setMessages(res.data.results)
      if (res.data.count > 0) {
        setTimeout(()=>{
          listRef.current?.scrollToIndex({index: 0, viewPosition: 0, viewOffset: 120})
        }, 200)

        // if any unread messages mark last message as read
        if (messages.filter((msg: any) => !msg.was_opened).length > 0) {
          markMessageAsRead(res.data.results[0].id)
        }
      }
    })
  }

  const sendMessage = () => {
    const data = JSON.stringify({
      connection: route.params.connectionId,
      sender_id: profile.id,
      text: messageText
    })

    axios.post(`${apiUrl}/messages/`, data, apiHeader(initialTokens.access_token)).then(res => {
      markMessageAsRead(res.data.id)
      setMessageText('')
      getAllMessages()
    })
  }

  return (
    <View style={styles.container}>
      <FlatList 
        data={messages}
        renderItem={(message: MessageInterface) => 
          <MessageListItem 
            message={message} 
            lastMessageDate={
              message.index !== messages.length - 1 ? 
              messages[message.index + 1].date_sent :
              ''
            } 
            profile={profile} 
          />
        }
        style={styles.flatList}
        ref={listRef}
        contentInset={{ top: 120 }}
        inverted={true}
      />
      <View style={styles.messageInputContainer}>
        <View style={styles.messageInput}>
          <TextInput 
            placeholder="Message..." 
            value={messageText}
            onChangeText={(text: any) => setMessageText(text)} 
            multiline={true}
            style={styles.input}
          />
          {!!messageText &&
            <TouchableOpacity 
              style={styles.sendBtn}
              onPress={sendMessage}
            >
              <Text style={styles.sendBtnText}>Send</Text>
            </TouchableOpacity>
          }
        </View>
      </View>
    </View>
  )
}

export default connector(MessageListScreen)

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    height: '100%',
  },
  flatList: {
    flex: 1,
    height: '100%',
  },
  messageInputContainer: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    paddingBottom: 16,
    paddingHorizontal: 16,
    backgroundColor: 'rgba(255,255,255,0.0)'
  },
  messageInput: {
    borderWidth: 1,
    borderColor: 'lightgrey',
    marginBottom: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    borderRadius: 16,
    paddingRight: 16,
    backgroundColor: 'rgba(255,255,255,0.9)'
  },
  input: {
    flex: 1,
    borderBottomWidth: 0,
  },
  sendBtn: {
    flex:0
  },
  sendBtnText:{
    color: 'blue'
  },
  messageContainerRight: {
    alignItems: 'flex-end',
    paddingVertical: 8,
    paddingHorizontal: 16
  },
  messageContainerLeft: {
    paddingVertical: 8,
    paddingHorizontal: 16
  },
  messageRight: {
    backgroundColor: '#cedced',
    maxWidth: '70%',
    borderRadius: 16,
    padding: 16
  },
  messageLeft: {
    backgroundColor: '#e6e6e6',
    maxWidth: '70%',
    borderRadius: 16,
    padding: 16,
  },
  dateTime: {
    width: '100%',
    alignItems: 'center',
    marginTop: 0,
    marginBottom: 32
  },
  dateTimeText: {
    fontSize: 10,
    color: '#666'
  }
})
