import axios from 'axios'
import React, { useState, useEffect } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { useIsFocused } from "@react-navigation/native"
import { NativeStackScreenProps } from '@react-navigation/native-stack'

import { apiUrl, apiHeader } from '../utils'
import MessageConnectionsList from '../components/MessageConnectionsList'
import { resetNotifications } from '../components/TaskComponent'

import { RootStackParamList } from '../types'
import { UserStateType } from '../types/userTypes'

const mapStateToProps = (state: { userReducer: UserStateType }) => {
  return {
    initialTokens: state.userReducer.initialTokens,
    profile: state.userReducer.profile,
    update: state.userReducer.update,
  }
}

const connector = connect(mapStateToProps)
type PropsFromRedux = ConnectedProps<typeof connector>
type OwnProps = NativeStackScreenProps<RootStackParamList, 'ConnectionListScreen'>
type Props = PropsFromRedux & OwnProps

function MessageListTab({ navigation, initialTokens, profile, update }: Props) {
  const [connections, setConnections] = useState([])
  const isFocused = useIsFocused()

  useEffect(() => {
    if(isFocused) {
      axios.get(`${apiUrl}/message-connections/`, apiHeader(initialTokens.access_token)).then(res => {
        setConnections(res.data.results)
        resetNotifications(profile.id, initialTokens.access_token)
      })
    } 
  },[update, isFocused])

  return (
    <MessageConnectionsList 
      connections={connections} 
      profile={profile} 
      initialTokens={initialTokens}
    /> 
  )
}

export default connector(MessageListTab)
