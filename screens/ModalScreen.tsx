import axios, { AxiosResponse } from 'axios'
import _ from 'lodash'
import { StatusBar } from 'expo-status-bar'
import React, { useState, useEffect } from 'react'
import { Platform, StyleSheet, TouchableOpacity, Image } from 'react-native'
import * as ImagePicker from 'expo-image-picker'
import { connect, ConnectedProps } from 'react-redux'
import { NativeStackScreenProps } from '@react-navigation/native-stack'

import { setProfile } from '../actions/user'
import { Text, View, ProfileEditField } from '../components/Themed'
import InstagramLoginInput from '../components/InstagramLoginInput'
import { apiUrl, apiHeader } from '../utils'
import { RootStackParamList } from '../types'
import { UserStateType } from  '../types/userTypes'

const mapStateToProps = (state: { userReducer: UserStateType }) => {
  return {
    initialTokens: state.userReducer.initialTokens,
    profile: state.userReducer.profile,
  }
}

const mapDispatchToProps = {
  setProfile
}

const connector = connect(mapStateToProps, mapDispatchToProps)
type PropsFromRedux = ConnectedProps<typeof connector>
type OwnProps = NativeStackScreenProps<RootStackParamList, 'ModalScreen'>
type Props = PropsFromRedux & OwnProps

function ModalScreen(props: Props) {
  const [avatar, setAvatar] = useState<string>('')
  const [name, setName] = useState('')
  const [username, setUsername] = useState('')
  const [bio, setBio] = useState('')
  const { navigation, route } = props // TODO: combine types into one to handle mapStateToProps
  const stateFunctions: any = {
    name: setName,
    username: setUsername,
    bio: setBio,
  }

  useEffect(() => {
    handleNavigationOnEditProfile(route.params)
  },[route.params])

  const  handleNavigationOnEditProfile = async (params: RootStackParamList['ModalScreen']) => {
    // user returning from Field Input and storing value in state
    if (params && params.inputValue) { 
      storeValueToStateUponReturn(params)
    }

    // when 'Done' button on rightHeader of modal is pressed, we...
    const dataHasChanged = [avatar, name, username, bio].some(( el => !!el ))
    if (params && params.save && dataHasChanged) {
      const isSaved = await save()
      if (isSaved) {
        navigation.navigate('Root')
      }
    } else if (params && params.save && !dataHasChanged){
      navigation.navigate('Root')
    }
  }

  const storeValueToStateUponReturn = (params: RootStackParamList['ModalScreen']) => {
    stateFunctions[params.inputName](params.inputValue)
  }

  const saveProfileData = (profileData: UserStateType['profile']) => {
    // save all user data
    return axios.patch(`${apiUrl}/profiles/${props.profile.id}/`, JSON.stringify(profileData), apiHeader(props.initialTokens.access_token))
    .catch(err => { console.log(err) })
  }

  const saveAvatar = (username: string) => {
    // save new avatar to user
    const extension = avatar.split('.').pop()
    const avatarImage: any = {
      uri: avatar,
      name: `${username}_avatar.${extension}`,
    }

    const body = new FormData()
    body.append('avatar', avatarImage)

    return axios.put(`${apiUrl}/profiles/${props.profile.id}/set_avatar/`, body, apiHeader(props.initialTokens.access_token))
  }

  const save = () => {
    const possibleFields = [
      ['name', name],
      ['username', username],
      ['bio', bio],
    ]

    let profileData: any = {}
    for (let fieldTuple of possibleFields) {
      if (fieldTuple[1]) {
        profileData[fieldTuple[0]] = fieldTuple[1]
      } 
    }

    const _formatAvatarName = (avatarResponse: { data: UserStateType['profile']}) => {
      const userObj = avatarResponse.data
      userObj.avatar = `${apiUrl}${userObj.avatar}`
      return userObj
    }

    // Since we make a PATCH call for string data and a PUT call for image data
    // we need to see which data has changed and make appropriate call
    if (!_.isEmpty(profileData) && avatar) {
      return saveProfileData(profileData).then(profileDataResponse => {
        if (profileDataResponse) {
          return saveAvatar(profileDataResponse.data.username).then((avatarResponse: AxiosResponse<UserStateType['profile']>) => {
            props.setProfile(_formatAvatarName(avatarResponse))
            return true
          }).catch(err => { console.log(err) })
        }
      }).catch(err => { console.log(err) })
    } else if (!_.isEmpty(profileData) && !avatar) {
      return saveProfileData(profileData).then(profileDataResponse => { 
        if (profileDataResponse) {
          props.setProfile(profileDataResponse.data)
          return true
        }
      }).catch(err => { console.log(err) })
    } else if (_.isEmpty(profileData) && avatar) { 
      return saveAvatar(props.profile.username).then((avatarResponse: AxiosResponse<UserStateType['profile']>) => {
        props.setProfile(_formatAvatarName(avatarResponse))
        return true
      }).catch(err => { console.log(err) })
    }
  }

  const pickAvatar = async () => {
    // No permissions request is necessary for launching the image library
    let result = await ImagePicker.launchImageLibraryAsync({
      presentationStyle: 8, // UIImagePickerPresentationStyle.BlurOverFullScreen
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [1, 1],
      quality: 1,
    })

    if (!result.cancelled) {
      setAvatar(result.uri)
    }
  }

  const handleImagePicker = () => {
    pickAvatar()
  }

  interface InputFieldInterface {
    name: string
    value: string
    save?: boolean
  }

  const InputFieldButton = (props: InputFieldInterface) => {
    const { name, value } = props
    const params = {
      name: name,
      value: value
    }
    return (
      <TouchableOpacity 
        onPress={() => navigation.navigate('ProfileEditInput', params)} 
        style={styles.inputButton}
      >
        <ProfileEditField name={name} value={value} />
      </TouchableOpacity>
    )
  }

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={handleImagePicker} style={styles.pickAvatar}>
        {avatar ? 
          <Image source={{ uri: avatar }} style={{ borderRadius: 100, width: 200, height: 200 }} /> :
          <Image source={{ uri: props.profile.avatar }} style={{ borderRadius: 100, width: 200, height: 200 }} /> 
        }
        <Text style={styles.pickAvatarText}>Change Profile Photo</Text>
      </TouchableOpacity>

      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />

      <InputFieldButton name="username" value={!!username ? username : props.profile.username} />
    
      <InputFieldButton name="bio" value={!!bio ? bio : props.profile.bio} />

      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />

      {/* Use a light status bar on iOS to account for the black space above the modal */}
      <StatusBar style={Platform.OS === 'ios' ? 'light' : 'auto'} />

      <InstagramLoginInput />
    </View>
  )
}

export default connector(ModalScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  pickAvatar: {
    alignItems: 'center',
    padding: 24
  },
  pickAvatarText: {
    paddingTop: 16
  },
  inputButton: {
    width: '100%'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 1,
    height: 1,
    width: '100%',
  },
})
