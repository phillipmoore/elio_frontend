import axios from 'axios'
import React, { useState, useEffect } from 'react'
import { useIsFocused } from "@react-navigation/native"
import { NativeStackScreenProps } from '@react-navigation/native-stack'

import { connect, ConnectedProps } from 'react-redux'
import { apiUrl, apiHeader } from '../utils'
import ProfileDetail from '../components/ProfileDetail'

import { RootStackParamList } from '../types'
import { UserStateType } from  '../types/userTypes'

const mapStateToProps = (state: { userReducer: UserStateType }) => {
  return {
    initialTokens: state.userReducer.initialTokens,
    profile: state.userReducer.profile
  }
}

const connector = connect(mapStateToProps)
type PropsFromRedux = ConnectedProps<typeof connector>
type OwnProps = NativeStackScreenProps<RootStackParamList, 'MyProfileDetailTab'>
type Props = PropsFromRedux & OwnProps


function MyProfileDetailTab({ navigation, profile, initialTokens }: Props) {
  const [myProfile, setMyProfile] = useState(profile)
  const isFocused = useIsFocused()

  useEffect(() => {
    // unfortunately, because of the connection_count field being on the model,
    // we have to reset the profile here in case new connections were made
    if (isFocused) {
      axios.get(`${apiUrl}/profiles/${profile.id}/`, apiHeader(initialTokens.access_token)).then(res =>{
        setMyProfile(res.data)
      }).catch(err => {
        console.log(err)
      })
    }
  }, [isFocused])

  return (
    <ProfileDetail 
      profile={myProfile}
      navigation={navigation} 
      isLoggedIn={true}
    />
  )
}

export default connector(MyProfileDetailTab)
