import React from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { NativeStackScreenProps } from '@react-navigation/native-stack'

import ProfileDetail from '../components/ProfileDetail'

import { RootStackParamList } from '../types'
import { UserStateType } from  '../types/userTypes'


const mapStateToProps = (state: { userReducer: UserStateType }) => {
  return {
    initialTokens: state.userReducer.initialTokens,
    profile: state.userReducer.profile
  }
}

const connector = connect(mapStateToProps)
type PropsFromRedux = ConnectedProps<typeof connector>
type OwnProps = NativeStackScreenProps<RootStackParamList, 'ProfileDetail'>
type Props = PropsFromRedux & OwnProps

function ProfileDetailScreen(props: Props) {
  const { navigation, route, profile } = props

  return (
    <ProfileDetail 
      profile={route.params.profile}
      navigation={navigation} 
      isLoggedIn={route.params.profile.id === profile.id}
    />
  )
}

export default connector(ProfileDetailScreen)
