import React, { useState, useEffect, useRef } from 'react'
import { StyleSheet } from 'react-native'
import { Text, View, TextInput } from '../components/Themed'

import { RootStackScreenProps } from '../types'

export default function ProfileEditInput({ navigation, route }: RootStackScreenProps<'ProfileEditInput'>) {
  const [inputValue, setInputValue] = useState('')
  const [inputName, setInputName] = useState('')
  const { name, value, save } = route.params
  const inputFieldRef = useRef<typeof TextInput>()

  useEffect(() => {
    // set value and name if navigating from EditProfile screen
    const isValue = value === "" || !!value
    if (isValue) {
      setInputValue(value)
    }
     setInputName(name)

    // waiting for the navigation animation to finish to attach focus to Input
    setTimeout(() => { 
      inputFieldRef?.current?.focus()
    }, 1000)

    // hitting the done button headerRight...
    if (save) {
      navigation.navigate('EditProfile', { inputName, inputValue})
    }

  },[route.params])

  return (
    <View style={styles.container}>
      <Text style={styles.label}>{inputName}</Text>
      <TextInput 
        defaultValue={inputValue} 
        onChangeText={(value: string) => setInputValue(value)}
        multiline={true}
        ref={inputFieldRef}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  label: {
    color: 'rgba(0,0,0,0.5)',
    paddingLeft: 16,
    paddingTop: 8,
    width: '100%',
    fontSize: 12
  }
})