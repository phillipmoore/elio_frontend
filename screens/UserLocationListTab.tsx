import axios from 'axios'
import _ from 'lodash'

import React, { useEffect, useState } from 'react'
import { StyleSheet, Switch, Dimensions } from 'react-native'
import { connect, ConnectedProps } from 'react-redux'
import { useIsFocused } from "@react-navigation/native"
import Carousel from 'react-native-snap-carousel'
import MapView from 'react-native-maps'

import { setProfile, setInitialTokens } from '../actions/user'
import { apiUrl, apiHeader } from '../utils'
import { Text, View } from '../components/Themed'
import LocationListItem from '../components/LocationListItem'

import { UserStateType } from '../types/userTypes' 
import { LocationInterface } from '../types/modelTypes' 

const windowWidth = Dimensions.get('window').width

interface ToggleVisibilityProps { 
  profile: UserStateType['profile']
  initialTokens: UserStateType['initialTokens']
  setProfile: (profile: object) => { type: string, data: object }
}

export const toggleVisibility = (props: ToggleVisibilityProps ) => {
    // ... of the logged-in users current profile
  const data = {
    is_visible: !props.profile.is_visible
  }
  return axios.patch(`${apiUrl}/profiles/${props.profile.id}/`, data, apiHeader(props.initialTokens.access_token)).then(res => {
    props.setProfile(res.data)
    return res.data
  })
}

const mapStateToProps = (state: { userReducer: UserStateType } ) => {
  return {
    initialTokens: state.userReducer.initialTokens,
    profile: state.userReducer.profile,
    update: state.userReducer.update,
    userLocation: state.userReducer.userLocation
  }
}

const mapDispatchToProps = {
  setProfile,
  setInitialTokens
}

const connector = connect(mapStateToProps, mapDispatchToProps)
type PropsFromRedux = ConnectedProps<typeof connector>
export type Props = PropsFromRedux 

const UserLocationListTab = (props: Props) => {
  const [locations, setLocations] = useState([]) // TODO: instead of empty array, import placeholder 
  const isFocused = useIsFocused()
  const cardHeight = windowWidth + 99 // picture height + top and bottom bar
  const { profile, initialTokens, setProfile } = props

  useEffect(() => {
    // when this tab is focused, update the locations list
    if (isFocused){
      getLocations()
    }

    console.log(profile)

  },[props.update, isFocused]) // updates with redux update and focus

  const getLocations = () => {
    axios.get(`${apiUrl}/locations/`, apiHeader(props.initialTokens.access_token)).then(res => {
      setLocations(res.data.results)
    }).catch(err => { console.log(err) })
  }

  const pp = locations.length === 1 ? 'person' : 'people'
  const ifLocations = locations.length > 0
  const heights = {
    map: ifLocations ? '34%' : '74%',
    locations: ifLocations ? '60%' : '20%',  
  }

  return (
    <View style={styles.container}>
    {/*{<TouchableOpacity onPress={()=>{ props.setProfile({}); props.setInitialTokens({}); }}><Text>LOGOUT</Text></TouchableOpacity>}*/}
      <View style={[styles.mapViewContainer, {height: heights.map}]}>
        <MapView 
          style={{height: '100%', width: windowWidth}} 
          initialRegion={{
            latitude: props.userLocation.latitude,
            longitude: props.userLocation.longitude,
            latitudeDelta: 0.001,
            longitudeDelta: 0.001,
          }}
          showsUserLocation={true}
          followsUserLocation={true}
        />
      </View>
      <View style={styles.activity}>
        <Text style={styles.activityLeft}>{locations.length} {pp} in your radius</Text>
        <Switch 
          trackColor={{ false: "#767577", true: "crimson" }}
          ios_backgroundColor="#3e3e3e"
          onValueChange={() => { toggleVisibility({ profile, initialTokens, setProfile })}}
          value={props.profile.is_visible}
        />
      </View>
      <View style={[styles.locationsList, {height: heights.locations}]}>
        { locations.length > 0 ?
          <Carousel
            // TODO: replace with reanimated Carousel. import Carousel from 'react-native-reanimated-carousel'
            data={locations}
            renderItem={(location: LocationInterface) => <LocationListItem location={location} getLocations={getLocations} />}
            snapToInterval={cardHeight}
            decelerationRate="fast"
            sliderWidth={windowWidth}
            itemWidth={windowWidth - 100}
          /> :
          <View style={styles.noLocations}>
            <Text style={styles.noLocationsText}>Find your people</Text>
          </View>
        }
      </View>
      {/*
        TODO: implement light and dark mode
        <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      */}
    </View>
  )
}

export default connector(UserLocationListTab)

const styles = StyleSheet.create({
  container: {
    height: '100%',
    justifyContent: 'space-between',
  },
  mapViewContainer: {
    height: '34%', 
    width: '100%',
  },
  activity: {
    height: '6%',
    width: '100%',
    paddingLeft: 8,
    paddingRight: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 0.25,
    borderBottomColor: 'lightgrey',
    borderTopWidth: 0.25,
    borderTopColor: 'lightgrey'
  },
  activityLeft: {
    // fontWeight: 'bold',
  },
  locationsList: {
    height: '60%',
    backgroundColor: '#ededed',
    justifyContent: 'center',
  },
  noLocations: {
    padding: 16,
    backgroundColor: '#ededed',
    alignItems: 'center'
  },
  noLocationsText: {
    color: 'lightgrey',
    fontSize: 32
  }
})
