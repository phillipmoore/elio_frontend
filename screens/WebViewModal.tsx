import React from 'react'
import { WebView as LoginWebView, WebViewNavigation } from 'react-native-webview'
import { NativeStackScreenProps } from '@react-navigation/native-stack'

import { igObj, redirectUrl, loginToIg } from '../resourceUtils'

import { RootStackParamList } from '../types'

type Props = NativeStackScreenProps<RootStackParamList, 'WebView'>

const WebViewModal = ({ navigation }: Props) => {

  const handleNavigation = (e: WebViewNavigation) => {
    // strictly for instagram at the moment, we are waiting for the user to navigate 
    // through to the end of the auth process and return a code to be used in the future
    if (e.url.startsWith(redirectUrl)) {
      const urlParts = e.url.split('?code=') // grabbing instagram code from query string
      loginToIg(urlParts[1]).then(res => {
        if (res) {
          navigation.navigate('EditProfile', { inputName: '', inputValue: '' })
        }
      })
    }
  }

  return (
    <LoginWebView 
      originWhitelist={['*']}
      source={{ uri: igObj.authorizationUrl }}
      onNavigationStateChange={(e: WebViewNavigation) => handleNavigation(e)}
    />
  )
}

export default WebViewModal
