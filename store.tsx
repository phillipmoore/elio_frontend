import {createStore, combineReducers} from 'redux';
import resourceReducer from './reducers/resourceReducer'
import userReducer from './reducers/userReducer'
import animationReducer from './reducers/animationReducer'


const rootReducer = combineReducers({
  resourceReducer: resourceReducer,
  userReducer: userReducer,
  animationReducer: animationReducer,
})

const store = createStore(rootReducer);

export default store;
