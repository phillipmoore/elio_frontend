import { ReactElement } from 'react'
import { ViewStyle } from 'react-native'

import { UserStateType } from './userTypes'
import { 
  ConnectionInterface, 
  LocationInterface, 
  NestedActionType 
} from './modelTypes'


export interface ComponentInterface {
  ConnectionList: {
    connections: ConnectionInterface['item'][] 
    mutualConnections: ConnectionInterface['item'][]
    profile: UserStateType['profile']
    nestedAction: (foo: NestedActionType) => void
    fromTabView: boolean
    tabIndex: number 
  }
  ConnectionListItem: {
    profile: UserStateType['profile'] 
    connection: ConnectionInterface 
    nestedAction: (foo: NestedActionType) => void 
    swipingCheck: (swiping: boolean) => void 
    fromTabView: boolean
  }
  Dot: {
    position?: ViewStyle
  }
  FadeInOutView: {
    isVisible: boolean
    style: ViewStyle
    children: ReactElement
  }
  LocationListItem: {
    location: LocationInterface
    getLocations: () => void
  }
  MessageConnectionsList: {
    connections: ConnectionInterface['item'][]  
    profile: UserStateType['profile'] 
    initialTokens:  UserStateType['initialTokens']
  }
  MessageConnectionsListItem: {
    connection: ConnectionInterface  
    profile: UserStateType['profile'] 
    initialTokens:  UserStateType['initialTokens']
  }
  ProfileAcionBar: {
    owner: UserStateType['profile']
    profile: UserStateType['profile'] 
    handleAction: (action: string) => void
  }
  ProfileDetail: {
    profile: UserStateType['profile']
    isLoggedIn: boolean
  }
  TabBarButton: {
    sectionName: string
    style: ViewStyle
  }
}
