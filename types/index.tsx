/**
 * Learn more about using TypeScript with React Navigation:
 * https://reactnavigation.org/docs/typescript/
 */

import { BottomTabScreenProps } from '@react-navigation/bottom-tabs'
import { CompositeScreenProps, NavigatorScreenParams } from '@react-navigation/native'
import { NativeStackScreenProps } from '@react-navigation/native-stack'

import { UserStateType } from './userTypes'


declare global {
  namespace ReactNavigation {
    interface RootParamList extends RootStackParamList {}
  }
}

export type RootStackParamList = {
  Root: NavigatorScreenParams<RootTabParamList> | undefined
  EditProfile: { inputName: string, inputValue: string }
  NotFound: undefined
  ProfileDetail: { profile: UserStateType['profile'] }
  ProfileEditInput: { name: string, value: string, save?: boolean }
  ModalNavigator: undefined
  ConnectionListScreen: { profile: UserStateType['profile'], tabIndex: number }
  MessageListScreen: { connectionId: number }
  ContextModal: { profile: UserStateType['profile'] }
  WebView: undefined
  ModalScreen: { save: boolean, inputName: string, inputValue: string }
  MyProfileDetailTab: undefined
}

export type RootStackScreenProps<Screen extends keyof RootStackParamList> = NativeStackScreenProps<
  RootStackParamList,
  Screen
>

export type RootTabParamList = {
  UserLocationList: undefined
  Messages: undefined
  MyProfileDetail: undefined
  Connections: undefined
}

export type RootTabScreenProps<Screen extends keyof RootTabParamList> = CompositeScreenProps<
  BottomTabScreenProps<RootTabParamList, Screen>,
  NativeStackScreenProps<RootStackParamList>
>

