import { UserStateType } from './userTypes'


export interface MessageInterface {
  index: number
  item: {
    id: number
    connection: ConnectionInterface['item']
    sender_id: number
    text: string
    date_sent: string
    was_opened: boolean
    was_liked: boolean
  }
}

export interface ConnectionInterface {
  index: number
  item: {
    id: number
    initiator: UserStateType['profile']
    responder: UserStateType['profile']
    accepted: boolean
    initiator_hidden: boolean
    responder_hidden: boolean
    last_message: MessageInterface['item']
  }
}

export interface ConnectionDataInterface {
  accepted?: boolean
  initiator_hidden?: boolean
  responder_hidden?: boolean
}


export interface LocationInterface {
  index: number
  item: {
    owner: UserStateType['profile']
    latitude: string
    longitude: string
    location_time: string
  }
}

export const COMPLETE_CONNECTION = 'complete-connection'
export const VISIT_PROFILE = 'visit-profile'
export const SHOW_HIDE_CONNECTION = 'show-hide-connection'

interface CompleteConnection {
  type: typeof COMPLETE_CONNECTION
  connection: ConnectionInterface['item']
}

interface VisitProfile {
  type: typeof VISIT_PROFILE
  otherProfile: number
}

interface ShowHideConnection {
  type: typeof SHOW_HIDE_CONNECTION
  connection: ConnectionInterface['item']
}

export type NestedActionType = 
  CompleteConnection | 
  VisitProfile | 
  ShowHideConnection

export const WEB_PAGE_RESOURCE = 'wp'
export const INSTAGRAM_RESOURCE = 'ig'
export const YOUTUBE_RESOURCE = 'yt'
 
interface WebPageResource {
  resource_type: typeof WEB_PAGE_RESOURCE
  profile: UserStateType['profile']
  url: string
}

interface InstagramResouce {
  resource_type: typeof INSTAGRAM_RESOURCE
  profile: UserStateType['profile']
  handle: string
  token: string
  token_expiration: string
}

interface YoutubeResource {
  resource_type: typeof YOUTUBE_RESOURCE
  profile: UserStateType['profile']
  handle: string
  token: string
  token_expiration: string
}

export type ResourceType =
  WebPageResource |
  InstagramResouce |
  YoutubeResource
