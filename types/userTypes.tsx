import { 
  SET_INITIAL_TOKENS, 
  SET_USER,
  SET_PROFILE,
  SET_UPDATE,
  SET_NOTIFICATIONS,
  SET_USER_LOCATION,
  SET_INSTAGRAM_OBJECT,
  SET_TOP_DATA,  
} from '../actions/types'

/* --------- State Types ---------- */

interface InitialTokensInterface {
  access_token: string
  expires_in: number
  refresh_token: string
}

interface UserInterface {
  id: number
  email: string
  name: string
  last_profile_id: number
}

interface ProfileInterface {
  id: number
  user: UserInterface
  username: string
  bio: string
  avatar: string
  connection_count: number
  mutual_count: number
  is_visible: boolean
  notifications: boolean
  blocked_profiles: number[]
  resource_order: number[] | null
  connection_to_user: {
    id: number 
    initiator: ProfileInterface
    accepted: boolean
  }
}

type UpdateInterface = Date

interface IdListInterface {
  id_list: string[]
}

interface NotificationsInterface {
  locations: IdListInterface 
  messages: IdListInterface 
  connections: IdListInterface
}

interface UserLocationInterface {
  latitude: number
  longitude: number 
  location_time: Date
}

export interface UserStateType {
  initialTokens: InitialTokensInterface
  user: UserInterface
  profile: ProfileInterface
  update: UpdateInterface
  notifications: NotificationsInterface
  userLocation: UserLocationInterface
}

/* --------- Action Types ---------- */

interface SetInitialTokensInterface {
  type: typeof SET_INITIAL_TOKENS
  data: InitialTokensInterface
}

interface SetUserInterface {
  type: typeof SET_USER
  data: UserInterface
}

export interface SetProfileInterface {
  type: typeof SET_PROFILE
  data: ProfileInterface
}

interface SetUpdateInterface {
  type: typeof SET_UPDATE
  data: UpdateInterface
}

interface SetNotificationsInterface {
  type: typeof SET_NOTIFICATIONS
  data: NotificationsInterface
}

interface SetUserLocationInterface {
  type: typeof SET_USER_LOCATION
  data: UserLocationInterface
}

export type UserActionType = 
  SetInitialTokensInterface | 
  SetUserInterface | 
  SetProfileInterface | 
  SetUpdateInterface | 
  SetNotificationsInterface | 
  SetUserLocationInterface
