import _ from 'lodash'
import AsyncStorage from '@react-native-async-storage/async-storage'
import store from './store'
import { Dimensions } from 'react-native'
import { setBottomScreenCoords, setTopScreenCoords } from './actions/animation'

export const apiUrl = 'http://127.0.0.1:8000'

export const PAD = 8 // using in the Profile section to maintain x/y coords of ContentDetailContainers
export const RADIUS = 24

// ------------ constants above, utils below ------------//

// TODO: grab these from 
export const clientId = 'fIipzD0d9FMqlPK4IasFlUA5ga6vGkWX9nCoTFJL'
export const clientSecret = 'S44ksXbw4bCHTDLkcpRkuHXoMCuSgo70wWBpTNmxRsy5IIlRnNJlwZZOggLeKVBhp5h4SCZYPdvzrzYdfgOnXRqiCWok8tXsUK5l4pEaEOJpoJhMYq4CKUbyfqVCecxI'

export const apiHeader = (accessToken: string) => {
  return {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${accessToken}`
    }
  }
}

export function isEmail(email: string) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

export const formatDate = (dateString: string) => {
	const nuDate = new Date(dateString)
	return nuDate.toDateString()
}

export const formatDateTime = (dateString: string) => {
  var options: any = { weekday: 'long', month: 'long', day: 'numeric', hour: '2-digit', minute:'2-digit' };
  const nuDate = new Date(dateString)
  return nuDate.toLocaleTimeString("en-US", options)
}

export const safeEmail = (email: string) => {
  return email.replaceAll('.', '<dot>')
}

export const storeData = async (key: string, value: object) => {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(value))
  } catch (e) {
    console.log(`Could not store ${key} with data:`, value)
  }
}

export const getData = async (key: string) => {
  try {
    const jsonValue = await AsyncStorage.getItem(key)
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch(e) {
    console.log(`Could not get ${key}`)
  }
}

export const getScrambledUri = (profile: any) => {
  const userFolder = `user_${profile.user.id}/`
  const splitUri = profile.avatar.split(userFolder)
  return `${splitUri[0]}${userFolder}scrambled_${splitUri[1]}`
}

export const randomizeString = (str: string) => {
  return str.split('').sort(function(){return 0.5-Math.random()}).join('').toLowerCase()
}

export const connectionTabIndices = {
  all: 0,
  mutual: 1
}

export const getNewNotifications = (oldData: any, newData: any) => {
  const keys = ['locations', 'messages', 'connections']
  const notifObj = _.cloneDeep(newData)

  for (const i in keys) {
    // removing the same list items
    _.pullAll(notifObj[keys[i]].id_list, oldData[keys[i]].id_list)
  }
  return notifObj
}

export const getShrunkenImage = (imageUrl: string, baseWidth: number) => {
  return `${apiUrl}/image_shrink/?url=${encodeURIComponent(imageUrl)}&base_width=${baseWidth}`
}

export const randomInt = () => {
  return Math.floor(Math.random() * 100000000)
}

export const getVisibleIGSource = (igItem: any) => {
  return igItem?.media_type !== 'VIDEO' ? igItem?.media_url : igItem?.thumbnail_url
}

export const expandToItem = (
    e: any, 
    index: number, 
    itemLayout: any,
    isToplevel: boolean
  ) => {

  const state = store.getState()
  const headerHeight = state.animationReducer.headerHeight
  const profileContentHeight = state.animationReducer.profileContentHeight
  const screenWidth = Dimensions.get('screen').width

  const el = e.nativeEvent
  const s2IRatio = screenWidth / itemLayout.width
  const aspectRatio = itemLayout.width / itemLayout.height

  const adjustTranslateX = ((screenWidth - (screenWidth * (1/s2IRatio)))/2) * s2IRatio
  const x = (s2IRatio *(el.pageX - el.locationX)) - adjustTranslateX
  const adjustTranslateY = (((profileContentHeight - (profileContentHeight * (1/s2IRatio)))/2) * s2IRatio)
  const y = (s2IRatio *((el.pageY - el.locationY) - headerHeight)) - adjustTranslateY
  const setScreenCoords = !isToplevel ? setBottomScreenCoords : setTopScreenCoords
  store.dispatch(setScreenCoords({index: index, x: x, y: y, aspectRatio: aspectRatio, s2IRatio: s2IRatio}))
}
